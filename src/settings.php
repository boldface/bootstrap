<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for controlling the site settings
 *
 * @since 1.0
 */
class settings {

  /**
   * Add actions and filters from the after_setup_theme hook
   *
   * @access public
   * @since  1.0
   */
  public function after_setup_theme() {
    global $content_width;
    if( ! isset( $content_width ) ) $content_width = 1200;
    \add_theme_support( 'html5', [ 'gallery' ] );
    \add_theme_support( 'automatic-feed-links' );
  }

  /**
   * Add actions and filters from the admin_init hook
   *
   * @access public
   * @since  1.0
   */
  public function admin_init() {
    \add_editor_style( 'assets/css/style.css' );
  }

  /**
   * Add actions and filters from the init hook
   *
   * @access public
   * @since  1.0
   */
  public function init() {
    \add_theme_support( 'custom-logo' );
    \add_theme_support( 'title-tag' );
    \add_theme_support( 'custom-background', [ 'default-color' => '#ffffff' ] );
    \add_post_type_support( 'page', 'excerpt' );
    \add_filter( 'script_loader_tag', [ $this, 'scriptLoaderTag' ], 10, 3 );
    \add_filter( 'style_loader_tag' , [ $this, 'styleLoaderTag'  ], 10, 4 );
    \add_filter( 'script_loader_src', [ $this, 'preRemoveVersionQueryVer' ] );
    \add_filter( 'script_loader_src', [ $this, 'removeVersionQueryVer' ], 10, 2 );
    \add_filter( 'style_loader_src' , [ $this, 'removeVersionQueryVer' ], 10, 2 );
  }

  /**
   * Add actions and filters from the login_init hook.
   *
   * @access public
   * @since  1.0
   */
  public function login_init() {
    \add_action( 'login_enqueue_scripts', [ $this, 'loginEnqueueScripts' ] );
    \add_filter( 'login_headertitle'    , [ $this, 'loginHeaderTitle'    ] );
    \add_filter( 'login_headerurl'      , [ $this, 'loginHeaderURL'      ] );
  }

  /**
   * Print inline CSS.
   *
   * @access public
   * @since  1.0
   */
  public function loginEnqueueScripts() {
    $logo       = \apply_filters( 'Boldface\Bootstrap\settings\loginLogo'      , '' );
    $background = \apply_filters( 'Boldface\Bootstrap\settings\backgroundImage', '' );

    if( '' === $logo && '' === $background ) return;

    if( '' !== $logo ) {
      printf( '
    <style>
      #login h1 a, .login h1 a {
        background-image: url( %1$s );
        height: 65px;
        width: 320px;
        background-size: auto 65px;
        background-repeat: no-repeat;
        }
      </style>',
        \esc_url( $logo )
      );
    }

    if( '' !== $background ) {
      printf( '
    <style>
      .login {
        background-image: url( %1$s );
        background-position: top center;
        background-repeat: no-repeat;
        background-size: cover;
      }
      .login form, .login .message, .login h1 {
        background: #ffffffee !important;
        border-radius: 5px;
      }
    </style>',
        $background
      );
    }

    printf( '
    <style>
      .login form, .login .message, .login h1 {
        background: #ffffffee !important;
        border-radius: 5px;
      }
    </style>
      ',
      $background
    );
  }

  /**
   * Return the login header title.
   *
   * @access public
   * @since  1.0
   *
   * @param string $title Login header title. Unused.
   *
   * @return string New login header title.
   */
  public function loginHeaderTitle( string $title ) : string {
    return \get_bloginfo( 'name' );
  }

  /**
   * Return the login header URL.
   *
   * @access public
   * @since  1.0
   *
   * @param string $url Login header URL. Unused.
   *
   * @return string New login header URL.
   */
  public function loginHeaderURL( string $url ) : string {
    return \get_home_url();
  }

  /**
   * Remove type from script tags.
   *
   * @access public
   * @since  1.0
   *
   * @param string $tag    The tag of the enqueued javascript file.
   * @param string $handle The handle of the enqueued javascript file.
   * @param string $src    The source of the enqueued javascript file.
   *
   * @return string The modified tag of the javascript file.
   */
  public function scriptLoaderTag( string $tag, string $handle, string $src ) : string {
    if( 'bootstrap' === $handle && 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js' === $src ) {
      $tag = str_replace( "></script>", " integrity='sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm' crossorigin='anonymous'></script>", $tag );
    }
    return str_replace( " type='text/javascript'", '', $tag );
  }

  /**
   * Add integrity and crossorigin attributes.
   *
   * @access public
   * @since  1.0
   *
   * @param string $tag    The tag of the enqueued stylesheet.
   * @param string $handle The handle of the enqueued stylesheet.
   * @param string $href   The href of the enqueued stylesheet.
   * @param string $media  The media of the enqueued stylesheet.
   *
   * @return string The modified tag of the enqueued stylesheet.
   */
  public function styleLoaderTag( string $tag, string $handle, string $href, string $media ) : string {
    $href = \remove_query_arg( 'ver', $href );
    if( 'bootstrap' === $handle && 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css' === $href ) {
      $tag = str_replace( "rel='stylesheet'", "rel='stylesheet' integrity='sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4' crossorigin='anonymous'", $tag );
    }
    return $tag;
  }

  /**
   * Cast src to string. Workaround for bug introduced in Gutenberg 3.9.0
   * where boolean was passed as $src.
   *
   * @access public
   * @since  1.1.1
   *
   * @param mixed $src    The src of the enqueued stylesheet or script.
   *
   * @return string The modified src of the enqueued stylesheet or script.
   */
  public function preRemoveVersionQueryVer( $src ) {
    return (string) $src;
  }

  /**
   * Remove version query arg.
   *
   * @access public
   * @since  1.0
   *
   * @param string $src    The src of the enqueued stylesheet or script.
   * @param string $handle The handle of the enqueued stylesheet or script.
   *
   * @return string The modified src of the enqueued stylesheet or script.
   */
  public function removeVersionQueryVer( string $src, string $handle ) : string {
    return \remove_query_arg( 'ver', $src );
  }

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    //* Re-register jQuery to use CDN instead of local copy on non-admin pages
    if( ! \is_admin() ) {
      \add_filter( 'script_loader_tag', [ $this, 'deferScripts' ], 10, 2 );
      if( \apply_filters( 'Boldface\Bootstrap\localAssets', false ) ) {
        \wp_deregister_script( 'jquery' );
        \wp_enqueue_script( 'jquery', 'https://code.jquery.com/jquery-3.2.1.min.js', [], null, true );
      }
    }
  }

  /**
   * Defer all enqueued javaScript files.
   *
   * @access public
   * @since  1.0
   *
   * @param string $tag    The tag of the enqueued javascript file.
   * @param string $handle The handle of the enqueued javascript file.
   *
   * @return string The modified tag of the javascript file.
   */
  public function deferScripts( string $tag, string $handle ) : string {
    /**
     * Filters which scripts should not be deferred.
     *
     * @since 1.0.0
     *
     * @param array The scripts that should *not* be deferred.
     */
    $scripts = \apply_filters( 'Boldface\Bootstrap\settings\doNotDeferScripts', [] );
    return in_array( $handle, $scripts ) ? $tag : str_replace( ' src', ' defer="defer" src', $tag );
  }
}
