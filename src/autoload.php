<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for autoloading the theme files
 *
 * @since 1.0
 */
class autoload {

  /**
   * Require the necessary file if the namespace is valid.
   *
   * @access public
   * @since  1.0
   */
  public function load( string $class ) {
    if( $this->isValidNamespace( $class ) ) {
      $this->require( $class );
    }
  }

  /**
   * Return whether the namespace is valid.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $class The class to autoload.
   *
   * @return bool Whehter the namespace is valid.
   */
  protected function isValidNamespace( string $class ) : bool {
    return 0 === strpos( $class, __NAMESPACE__ );
  }

  /**
   * Require the file to autoload. Or die.
   *
   * @access protected
   * @since  1.0
   *
   * @param string The class to autoload.
   */
  protected function require( string $class ) {
    $file = $this->getFilename( $class );
    if( file_exists( $file ) ) {
      require $file;
      return;
    }
    $this->die( $file );
  }

  /**
   * Return the filename of the class to autoload.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $class The class to autoload.
   *
   * @return string The absoute path to the file to autoload.
   */
  protected function getFilename( string $class ) : string {
    $src = __DIR__;
    $class = str_replace( __NAMESPACE__, '', $class );
    $class = str_replace( '\\', '/', $class );
    $class = str_replace( '/Controllers/', '/controllers/', $class );
    $class = str_replace( '/Models/', '/models/', $class );
    $class = str_replace( '/Views/', '/views/', $class );
    return __DIR__ . $class . '.php';
  }

  /**
   * Die.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $file The filename that doesn't exist.
   */
  protected function die( string $file ) {
    \wp_die( sprintf( '%1$s doesn\'t exists', $file ) );
  }
}
