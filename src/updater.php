<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for updating the theme
 *
 * @since 1.0
 */
class updater {

  /**
   * @var string Theme slug.
   *
   * @access protected
   * @since  1.0
   */
  protected $theme = 'bootstrap';

  /**
   * @var string Theme repository name.
   *
   * @access protected
   * @since  1.0
   */
  protected $repository = 'boldface/bootstrap';

  /**
   * @var string Repository domain.
   *
   * @access protected
   * @since  1.0
   */
  protected $domain = 'https://gitlab.com/';

  /**
   * @var string CSS endpoint for repository.
   *
   * @access protected
   * @since  1.0
   */
  protected $css_endpoint = '/raw/master/style.css';

  /**
   * @var string ZIP endpoint for repository.
   *
   * @access protected
   * @since  1.0
   */
  protected $zip_endpoint = '/repository/archive.zip';

  /**
   * @var string Remote CSS URI.
   *
   * @access protected
   * @since  1.0
   */
  protected $remote_css_uri;

  /**
   * @var string Remote ZIP URI.
   *
   * @access protected
   * @since  1.0
   */
  protected $remote_zip_uri;

  /**
   * @var string Remote version.
   *
   * @access protected
   * @since  1.0
   */
  protected $remote_version;

  /**
   * @var string Local version.
   *
   * @access protected
   * @since  1.0
   */
  protected $local_version;

  /**
   * Method called from the admin_init hook to initiate the updater
   *
   * @access public
   * @since  1.0
   * @since  1.1.0 Deprecated method. Use `init()` instead.
   */
  public function admin_init() {
    trigger_error( sprintf( 'The %1$s theme updater should initiate using the init hook and init method.', $this->theme ) );
    $this->init();
  }

  /**
   * Method called from the init hook to initiate the updater.
   *
   * @access public
   * @since  1.1.0
   */
  public function init() {
    $this->local_version = ( \wp_get_theme( $this->theme ) )->get( 'Version' );
    \add_filter( 'auto_update_theme', [ $this, 'auto_update_theme' ], 20, 2 );
    \add_filter( 'pre_set_site_transient_update_themes', [ $this, 'pre_set_site_transient_update_themes' ] );
    \add_filter( 'upgrader_source_selection', [ $this, 'upgrader_source_selection' ], 10, 4 );
  }

  /**
   * Method called from the auto_update_theme hook.
   *
   * @access public
   * @since  1.1.0
   *
   * @param bool      $update Whether to auto update the theme.
   * @param \stdClass $item The update offer.
   *
   * @return bool Whether to auto update the theme.
   */
  public function auto_update_theme( bool $update, \stdClass $item ) : bool {
    return $this->theme === $item->theme ? $this->shouldAutoUpdate( $item ) : $update;
  }

  /**
   * Rename the unzipped folder to be the same as the existing folder
   *
   * @access public
   * @since  1.0
   *
   * @param string       $source        File source location
   * @param string       $remote_source Remote file source location
   * @param \WP_Upgrader $upgrader      \WP_Upgrader instance
   * @param array        $hook_extra    Extra arguments passed to hooked filters
   *
   * @return string | \WP_Error The updated source location or a \WP_Error object on failure
   */
  public function upgrader_source_selection( string $source, string $remote_source, \WP_Upgrader $upgrader, array $hook_extra ) {
    global $wp_filesystem;

    $update = [ 'update-selected', 'update-selected-themes', 'upgrade-theme', 'update-theme' ];

    if( ! isset( $_REQUEST[ 'action' ] ) || ! in_array( $_REQUEST[ 'action' ], $update, true ) ) {
      return $source;
    }

    if( ! isset( $source, $remote_source ) ) {
      return $source;
    }

    if( false === stristr( basename( $source ), $this->theme ) ) {
      return $source;
    }

    $basename = basename( $source );
    $upgrader->skin->feedback( esc_html_e( 'Renaming theme directory.', 'bootstrap' ) );
    $corrected_source = str_replace( $basename, $this->theme, $source );

    if( $wp_filesystem->move( $source, $corrected_source, true ) ) {
      $wp_filesystem->delete( $corrected_source . 'tests', true, 'd' );
      $upgrader->skin->feedback( esc_html_e( 'Rename successful.', 'bootstrap' ) );
      return $corrected_source;
    }

    return new \WP_Error();
  }

  /**
   * Add respoinse to update transient if theme has an update.
   *
   * @access public
   * @since  1.0
   *
   * @param $transient
   *
   * @return
   */
  public function pre_set_site_transient_update_themes( $transient ) {
    if( $this->hasUpdate() ) {
      $response = [
        'theme'       => $this->theme,
        'new_version' => $this->remote_version,
        'url'         => $this->construct_repository_uri(),
        'package'     => $this->construct_remote_zip_uri(),
        'branch'      => 'master',
      ];
      $transient->response[ $this->theme ] = $response;
    }

    return $transient;
  }

  /**
   * Return whether the theme should be updated; when it's not a major version update.
   *
   * @access protected
   * @since  1.1.0
   *
   * @param \stdClass $item The update offer.
   *
   * @return bool Whether to update the theme.
   */
  protected function shouldAutoUpdate( \stdClass $item ) : bool {
    return $this->majorVersion( $this->local_version ) === $this->majorVersion( $item->new_version );
  }

  /**
   * Return the major version of the version string.
   *
   * @access protected
   * @since  1.1.0
   *
   * @param string $version The version string.
   *
   * @return bool The major version.
   */
  protected function majorVersion( string $version ) : string {
    return explode( '.', $version )[0];
  }

  /**
   * Construct and return the URI to the remote stylesheet
   *
   * @access protected
   * @since  1.0
   *
   * @return string The remote stylesheet URI
   */
  protected function construct_remote_stylesheet_uri() : string {
    return $this->remote_css_uri = $this->domain . $this->repository . $this->css_endpoint;
  }

  /**
   * Construct and return the URI to the remote ZIP file
   *
   * @access protected
   * @since  1.0
   *
   * @return string The remote ZIP URI
   */
  protected function construct_remote_zip_uri() : string {
    return $this->remote_zip_uri = $this->domain . $this->repository . $this->zip_endpoint;
  }

  /**
   * Construct and return the URI to remote repository
   *
   * @access protected
   * @since  1.0
   *
   * @return string The remote repository URI
   */
  protected function construct_repository_uri() : string {
    return $this->repository_uri = $this->domain . \trailingslashit( $this->repository );
  }

  /**
   * Get and return the remote version
   *
   * @access protected
   * @since  1.0
   *
   * @return string The remote version
   */
  protected function get_remote_version() : string {
    $this->remote_stylesheet_uri = $this->construct_remote_stylesheet_uri();
    $response = $this->remote_get( $this->remote_stylesheet_uri );
    $response = str_replace( "\r", "\n", \wp_remote_retrieve_body( $response ) );
    $headers = [ 'Version' => 'Version' ];

    foreach( $headers as $field => $regex ) {
      if( preg_match( '/^[ \t\/*#@]*' . preg_quote( $regex, '/' ) . ':(.*)$/mi', $response, $match ) && $match[1] ) {
        $headers[ $field ] = _cleanup_header_comment( $match[1] );
      }
      else {
        $headers[ $field ] = '';
      }
    }

    return $this->remote_version = ( '' === $headers[ 'Version' ] ) ? '' : $headers[ 'Version' ];
  }

  /**
   * Return whether the theme has an update
   *
   * @access protected
   * @since  1.0
   *
   * @return bool Whether the theme has an update
   */
  protected function hasUpdate() : bool {
    if( ! $this->remote_version ) $this->remote_version = $this->get_remote_version();
    return version_compare( $this->remote_version, $this->local_version, '>' );
  }

  /**
   * Wrapper for \wp_remote_get()
   *
   * @access protected
   * @since  1.0
   *
   * @param string $url  The URL to get
   * @param array  $args Array or arguments to pass through to \wp_remote_get()
   *
   * @return array|WP_Error Return the request or an error object
   */
  protected function remote_get( string $url, array $args = [] ) {
    return \wp_remote_get( $url, $args );
  }
}
