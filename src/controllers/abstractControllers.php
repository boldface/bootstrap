<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Abstract controller class
 *
 * @since 1.0
 */
abstract class abstractControllers {

  /**
   * @var The model object
   *
   * @access protected
   * @since  1.0
   */
  protected $model;

  /**
   * @var HTML render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = -1;

  /**
   * Class constructor
   *
   * @access public
   * @since  1.0
   *
   * @param $model The model
   */
  public function __construct( $model ) {
    $class = str_replace( 'Controllers', 'Models', __CLASS__ );
    $this->model = $model instanceof $class ? $model : null;
  }

  /**
   * Return the model.
   *
   * @access public
   * @since  1.0
   *
   * @return The model.
   */
  public function getModel() {
    return $this->model;
  }

  /**
   * Return the view.
   *
   * @access public
   * @since  1.0
   *
   * @return The view.
   */
  public function getView() {
    return $this->model->getView();
  }

  /**
   * Return the render priority.
   *
   * @access public
   * @since  1.0
   *
   * @return int The render priority.
   */
  public function getPriority() : int {
    return intval( $this->priority );
  }

  /**
   * Return the render callback.
   *
   * @access public
   * @since  1.2.0
   *
   * @return callable The render callback.
   */
  public function getCallback() {
    return \apply_filters( get_called_class() . '\callback', [ $this->getView(), 'html' ] );
  }
}
