<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the securityHeaders.
 *
 * @since 1.0
 */
class securityHeaders extends abstractControllers {

  /**
   * @var Security Headers
   *
   * @access protected
   * @since  1.0
   */
  protected $securityHeaders = [
    'contentSecurityPolicy',
    'strictTransportSecurity',
    'xFrameOptions',
    'xssProtection',
    'xContentTypeOptions',
    'referrerPolicy',
  ];

  /**
   * Add actions and filters from the wp_loaded hook.
   *
   * @access public
   * @since  1.0
   */
  public function wp_loaded() {
    $contentSecurityPolicies = \apply_filters( 'Boldface\Bootstrap\Controllers\securityHeaders\callbacks', [
      'default-src' => [ $this->model, 'self'   ],
      'script-src'  => [ $this->model, 'script' ],
      'style-src'   => [ $this->model, 'style'  ],
      'font-src'    => [ $this->model, 'font'   ],
      'form-action' => [ $this->model, 'self'   ],
    ] );

    if( empty( $contentSecurityPolicies ) ) return;

    foreach( $contentSecurityPolicies as $contentSecurityPolicy => $callback )
      is_callable( $callback ) && \add_action( 'Boldface\Bootstrap\Models\securityHeaders\CSP\\' . $contentSecurityPolicy, $callback );
  }

  /**
   * Add actions and filters from the init hook.
   *
   * @access public
   * @since  1.0
   */
  public function init() {
    array_map( function( $header ) {
      \add_action( 'send_headers', [ $this->model, $header ] );
    }, \apply_filters( 'Boldface\Bootstrap\Controllers\securityHeaders\headers', $this->securityHeaders ) );
  }
}
