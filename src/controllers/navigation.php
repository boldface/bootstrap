<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Class for controlling the navigation
 *
 * @since 1.0
 */
class navigation extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 5;

  /**
   * Add actions and filters on the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'nav_menu_css_class', [ $this->model, 'nav_menu_items' ] );
    \add_filter( 'nav_menu_submenu_css_class', [ $this->model, 'nav_menu_submenu_css_class' ] );
    \add_filter( 'nav_menu_link_attributes', [ $this->model, 'nav_menu_link_attributes' ] );
    \add_filter( 'nav_menu_item_id', [ $this->model, 'nav_menu_item_id' ] );

    \add_filter( 'Boldface\Bootstrap\Views\navigation\brand', [ $this->model, 'brand' ] );
    \add_filter( 'Boldface\Bootstrap\Views\navigation\menu', [ $this->model, 'menu' ] );
  }

  /**
   * Add actions and filters on the after_setup_theme hook
   *
   * @access public
   * @since  1.0
   */
  public function after_setup_theme() {
    \add_action( 'init', [ $this->model, 'registerNavMenus' ] );
  }
}
