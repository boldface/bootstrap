<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the jumbotron
 *
 * @since 1.0
 */
class jumbotron extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 20;

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_action( 'Boldface\Bootstrap\Views\jumbotron', [ $this->getView(), 'jumbotronHeader' ] );
  }
}
