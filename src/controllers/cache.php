<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the cache
 *
 * @since 1.0
 */
class cache extends abstractControllers {

  /**
   * Add actions on the wp_loaded hook
   *
   * @access public
   * @since  1.0
   */
  public function wp_loaded() {
    if( \is_admin() ) return;

    \add_action( 'Boldface\Bootstrap\Views\header'         , [ $this->model, 'ob_start'     ] );
    \add_action( 'Boldface\Bootstrap\Views\footer\shutdown', [ $this->model, 'ob_get_clean' ] );
  }

  /**
   * Do the cache.
   *
   * @access public
   * @since  1.0
   */
  public function after_setup_theme() {
    if( \is_admin() ) return;
    $this->model->doCache();
  }
}
