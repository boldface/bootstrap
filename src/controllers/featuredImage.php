<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the featuredImage image
 *
 * @since 1.0
 */
class featuredImage extends abstractControllers {

  /**
   * featuredImage controllers fired on the after_setup_theme hook
   *
   * @access public
   * @since  1.0
   */
  public function after_setup_theme() {
    \add_theme_support( 'post-thumbnails' );
  }

  /**
   * featuredImage controllers fired on the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    if( ! $this->model->hasfeaturedImage() ) return;

    /**
     * Filters the callback to display the featuredImage.
     *
     * @since 1.0.0
     *
     * @param callable The callback to display the featuredImage.
     * @param \Boldface\Bootstrap\Controllers\featuredImage `$this` object.
     */
    $callback = \apply_filters( 'Boldface\Bootstrap\Controllers\featuredImage\callback', [ $this, 'defaultCallback' ], $this );
    \add_action( 'wp', $callback, 20 );
  }

  /**
   * The default callback to display the featuredImage
   *
   * @access public
   * @since  1.0
   */
  public function defaultCallback() {
    if( ! \is_singular() ) return;
    \add_filter( 'post_class', [ $this->model, 'postClass' ] );
    \add_action( 'Boldface\Bootstrap\Views\loop\start', [ $this->getView(), 'defaultImage' ], 20 );
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage', 'get_the_post_thumbnail' );
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage\wrapper', [ $this->model, 'wrapperClass' ] );
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage\class', [ $this->model, 'featuredImageClass' ] );
  }
}
