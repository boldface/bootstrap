<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Class for controlling the footer
 *
 * @since 1.0
 */
class footer extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 99;

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_action( 'wp_enqueue_scripts', [ $this->model, 'enqueueScripts' ] );
    \add_action( 'wp_footer', [ $this->model, 'footer' ] );
    \add_filter( 'Boldface\Bootstrap\Views\footer', [ $this->getView(), 'footer' ] );
    \add_filter( 'Boldface\Bootstrap\Models\footer\css', [ $this->getView(), 'formatCSS' ], 99 );
  }
}
