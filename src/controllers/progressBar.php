<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the progressBar
 *
 * @since 1.2.0
 */
class progressBar extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.2.0
   */
  protected $priority = 1;

  /**
   * Add actions and filters from the wp hook.
   *
   * @access public
   * @since  1.2.0
   */
  public function wp() {
    \add_action( 'wp_enqueue_scripts', [ $this->model, 'enqueueScripts' ] );
  }
}
