<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the widgets
 *
 * @since 1.0
 */
class widgets extends abstractControllers {

  /**
   * Add actions from the after_setup_theme hook.
   *
   * @access private
   * @since  1.0
   */
  public function after_setup_theme() {
    \add_action( 'init', [ $this->model, 'registerSidebars' ] );
    \add_action( 'wp',   [ $this->model, 'processCallbacks' ] );
  }
}
