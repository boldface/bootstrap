<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the googleFonts
 *
 * @since 1.0
 */
class googleFonts extends abstractControllers {

  /**
   * googleFonts controllers fired on the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    $this->model->setFonts();
    if( $this->getView()->hasFonts() ) {
      \add_action( 'wp_footer', [ $this->getView(), 'link' ] );
    }
  }
}
