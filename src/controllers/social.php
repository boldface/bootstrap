<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the social.
 *
 * @since 1.0
 */
class social extends abstractControllers {

  /**
   * Add actions and filters from the admin_init hook.
   *
   * @access public
   * @since  1.0
   */
  public function admin_init() {
    \add_action( 'show_user_profile', [ $this->getView(), 'show_user_profile' ] );
    \add_action( 'edit_user_profile', [ $this->getView(), 'show_user_profile' ] );

    \add_action( 'personal_options_update', [ $this->model, 'profile_update' ] );
    \add_action( 'edit_user_profile_update', [ $this->model, 'profile_update' ] );
  }

  /**
   * Add actions and filters from the init hook.
   *
   * @access public
   * @since  1.0
   */
  public function init() {
    $hook = \is_admin() ? 'admin_init' : 'wp_head';
    \add_action( $hook, [ $this->model, 'setUpSocial' ] );
  }

  /**
   * Add actions and filters from the wp hook.
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'Boldface\Bootstrap\Views\social', [ $this->getView(), 'social' ] );

    if( \is_author() ) {
      \add_action( 'Boldface\Bootstrap\Views\loop\end', [ $this->getView(), 'html' ] );
    }
  }
}
