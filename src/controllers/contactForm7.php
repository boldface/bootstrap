<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for contactForm7
 *
 * @since 1.0
 */
class contactForm7 extends abstractControllers {

  /**
   * contactForm7 settings that need to be set on the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_action( 'wp_enqueue_scripts', [ $this->model, 'dequeue' ] );
    \add_action( 'wpcf7_form_class_attr', [ $this->model, 'class_attr' ] );
    \add_filter( 'Boldface\Bootstrap\Views\contactForm7\elements', [ $this->model, 'elements' ] );
    \add_filter( 'wpcf7_form_elements', [ $this->getView(), 'elements' ] );
  }

  /**
   * contactForm7 settings that need to be set on the rest_api_init hook
   *
   * @access public
   * @since  1.0
   */
  public function rest_api_init() {
    \add_action( 'wpcf7_form_class_attr', [ $this->model, 'class_attr' ] );
    \add_filter( 'Boldface\Bootstrap\Views\contactForm7\elements', [ $this->model, 'elements' ] );
    \add_filter( 'wpcf7_form_elements', [ $this->getView(), 'elements' ] );
  }
}
