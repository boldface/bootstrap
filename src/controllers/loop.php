<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Class for controlling the loop
 *
 * @since 1.0
 */
class loop extends abstractControllers {

  /**
   * @var Model of the loop
   *
   * @access protected
   * @since  1.0
   */
  protected $model;

  /**
   * @var Priority of the loop
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 50;

  /**
   * Add actions and filters from the wp hook
   *
   * @access protected
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'Boldface\Bootstrap\Views\loop', [ $this->model, 'loop' ] );
    \add_action( 'Boldface\Bootstrap\Views\loop\end', [ $this->model, 'postsPagination' ] );
    \add_filter( 'navigation_markup_template', [ $this->model, 'postsPaginationTemplate' ], 10, 2 );

    \add_filter( 'Boldface\Bootstrap\Views\loop\title', [ $this->getView(), 'rowWrap' ], 100 );

    if( ! \is_singular() ) {
      \add_filter( 'Boldface\Bootstrap\Views\loop\title', [ $this->model, 'pageHeading' ] );
      \add_filter( 'Boldface\Bootstrap\Views\loop\start', [ $this->getView(), 'pageTitle' ] );
    }

    if( \is_archive() ) {
      \is_category() &&  \add_filter( 'Boldface\Bootstrap\Model\loop\heading', [ $this->model, 'categoryTitle' ] );
      \is_tag() &&  \add_filter( 'Boldface\Bootstrap\Model\loop\heading', [ $this->model, 'tagTitle' ] );
    }

    if( \is_search() ) {
      \add_filter( 'Boldface\Bootstrap\Model\loop\heading', [ $this->model, 'searchHeading' ] );
    }

    if( \is_author() ) {
      \add_filter( 'Boldface\Bootstrap\Model\loop\heading', [ $this->model, 'authorHeading' ] );
    }

    if( \is_home() ) {
      \add_filter( 'Boldface\Bootstrap\Model\loop\heading', [ $this->model, 'BlogHeading' ] );
    }

    if( \is_404() ) {
      \add_action( 'Boldface\Bootstrap\Views\loop\start', [ $this->getView(), 'pageTitle' ] );
      \add_filter( 'Boldface\Bootstrap\Views\loop\title', [ $this->model, 'pageHeading' ] );
      \add_filter( 'Boldface\Bootstrap\Model\loop\heading', [ $this->model, 'errorTitle' ] );
      \add_action( 'Boldface\Bootstrap\Views\loop\end', [ $this->getView(), 'pageBody' ] );
      \add_filter( 'Boldface\Bootstrap\Views\loop\body', [ $this->model, 'errorMessage' ] );
    }
  }
}
