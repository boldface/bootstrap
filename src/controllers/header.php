<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Class for controlling the header
 *
 * @since 1.0
 */
class header extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 0;

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'body_class', [ $this->model, 'bodyClass' ] );

    if( \is_singular() ) {
      \add_filter( 'Boldface\Bootstrap\Views\header\author', [ $this->model, 'singularAuthor' ] );
      \add_filter( 'Boldface\Bootstrap\Views\header\description', [ $this->model, 'singularDescription' ] );
    }

    if( \is_category() ) {
      \add_filter( 'Boldface\Bootstrap\Views\header\author', [ $this->model, 'defaultAuthor' ] );
      \add_filter( 'Boldface\Bootstrap\Views\header\description', [ $this->model, 'categoryDescription' ] );
    }

    if( \is_tag() ) {
      \add_filter( 'Boldface\Bootstrap\Views\header\author', [ $this->model, 'defaultAuthor' ] );
      \add_filter( 'Boldface\Bootstrap\Views\header\description', [ $this->model, 'tagDescription' ] );
    }

    if( \is_home() || \is_front_page() ) {
      \add_filter( 'Boldface\Bootstrap\Views\header\author', [ $this->model, 'defaultAuthor' ] );
      \add_filter( 'Boldface\Bootstrap\Views\header\description', [ $this->model, 'blogDescription' ] );
    }

    if( \is_404() ) {
      \add_filter( 'Boldface\Bootstrap\Views\header\author', [ $this->model, 'defaultAuthor' ] );
      \add_filter( 'Boldface\Bootstrap\Views\header\description', [ $this->model, 'errorDescription' ] );
    }
  }

  /**
   * Add actions and filters from the init hook
   *
   * @access public
   * @since  1.0
   */
  public function init() {
    \add_action( 'login_enqueue_scripts', [ $this->getView(), 'loginEnqueueScripts' ] );
  }
}
