<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the customHeader
 *
 * @since 1.0
 */
class customHeader extends abstractControllers {

  /**
   * Controllers fired on the init hook
   *
   * @access public
   * @since  1.0
   */
  public function init() {
    \add_theme_support( 'custom-header' );
  }

  /**
   * Controllers fired on the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    if( \has_custom_header() ) {
      \add_filter( 'Boldface\Bootstrap\Views\loop', [ $this->getView(), 'customHeader' ] );
    }
  }
}
