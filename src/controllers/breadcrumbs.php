<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the breadcrumbs
 *
 * @since 1.0
 */
class breadcrumbs extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 20;

  /**
   * Class constructor
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_action( 'Boldface\Bootstrap\Views\breadcrumbs', [ $this->model, 'breadcrumbs' ] );
  }
}
