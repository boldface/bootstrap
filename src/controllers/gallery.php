<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the gallery
 *
 * @since 1.0
 */
class gallery extends abstractControllers {

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'post_gallery', [ $this->model, 'gallery' ], 5, 3 );
    \add_filter( 'Boldface\Bootstrap\Views\gallery\id', [ $this->model, 'galleryID' ] );
    \add_filter( 'Boldface\Bootstrap\Views\gallery\class', [ $this->model, 'galleryClass' ] );
    \add_filter( 'Boldface\Bootstrap\Views\gallery', [ $this->model, 'galleryItems' ] );

    \add_filter( 'Boldface\Bootstrap\Views\gallery\img\src', [ $this->model, 'galleryImgSrc' ] );
    \add_filter( 'Boldface\Bootstrap\Views\gallery\img\alt', [ $this->model, 'galleryImgAlt' ] );
    \add_filter( 'Boldface\Bootstrap\Views\gallery\title', [ $this->model, 'galleryTitle' ] );
    \add_filter( 'Boldface\Bootstrap\Views\gallery\text', [ $this->model, 'galleryText' ] );
  }
}
