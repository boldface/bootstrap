<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the popovers
 *
 * @since 1.0
 */
class popovers extends abstractControllers {

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_action( 'wp_enqueue_scripts', [ $this->model, 'addInlineScript' ] );
  }
}
