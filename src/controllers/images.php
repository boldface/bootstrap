<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the images
 *
 * @since 1.0
 */
class images extends abstractControllers {

  /**
   * Add actions and filters from the admin_init hook
   *
   * @access public
   * @since  1.0
   */
  public function admin_init() {
    \add_filter( 'get_image_tag_class', [ $this->model, 'get_image_tag_class' ], 10, 4 );
  }

  /**
   * Add actions and filters from the init hook
   *
   * @access public
   * @since  1.0
   */
  public function init() {
    \add_filter( 'img_caption_shortcode', [ $this->model, 'img_caption_shortcode' ], 10, 3 );
  }

  /**
  * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'wp_get_attachment_image_attributes', [ $this->model, 'wp_get_attachment_image_attributes' ], 10, 3 );
  }
}
