<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the article entries
 *
 * @since 1.0
 */
class entry extends abstractControllers {

  /**
   * Entry controllers fired on the WP hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    if( \apply_filters( 'Boldface\Bootstrap\Controllers\entry\autorow', true ) ) {
      \remove_filter( 'the_content', 'wpautop' );
      \add_filter( 'the_content', 'wpautop', 99 );
      \add_filter( 'the_content', [ $this->model, 'autoRow' ], 100 );
    }
    \add_filter( 'post_class', [ $this->model, 'post_class' ] );

    \add_action( 'Boldface\Bootstrap\Views\loop\items', [ $this->getView(), 'entry' ] );

    \add_filter( 'Boldface\Bootstrap\Views\entry', [ $this->getView(), 'entryHeader' ], 5 );
    \add_filter( 'Boldface\Bootstrap\Views\entry', [ $this->getView(), 'entryContent' ], 15 );
    \add_filter( 'Boldface\Bootstrap\Views\entry', [ $this->getView(), 'entryFooter' ], 25 );
    \add_filter( 'Boldface\Bootstrap\Views\entry\class', [ $this->model, 'entryClass' ] );

    \add_filter( 'Boldface\Bootstrap\Views\entry\header', [ $this->getView(), 'entryHeading' ] );
    \add_filter( 'Boldface\Bootstrap\Views\entry\heading', [ $this->model, 'header' ] );
    \add_filter( 'Boldface\Bootstrap\Views\entry\content', [ $this->model, 'content' ] );

    \add_filter( 'Boldface\Bootstrap\Views\entry\header', [ $this->getView(), 'rowWrap' ], 100 );
    \add_filter( 'Boldface\Bootstrap\Views\entry\footer', [ $this->getView(), 'rowWrap' ], 100 );

    if( ! \is_front_page() || \is_home() ) {
      \add_filter( 'Boldface\Bootstrap\Views\entry\footer', [ $this->model, 'tags' ] );
      \add_filter( 'Boldface\Bootstrap\Views\entry\footer', [ $this->model, 'categories' ] );
    }

    if( \is_singular() ) {
      \add_filter( 'comment_form_field_comment', [ $this->getView(), 'comment_form_field_comment' ] );
      \add_filter( 'Boldface\Bootstrap\Views\entry\footer', [ $this->model, 'linkPages' ] );
      \add_filter( 'Boldface\Bootstrap\Views\entry\footer', [ $this->getView(), 'listComments' ] );
      \add_filter( 'Boldface\Bootstrap\Views\entry\footer', [ $this->getView(), 'comment_form' ] );
    }
  }

  /**
   * Entry controllers fired on the rest_api_init hook.
   *
   * @access public
   * @since  1.0
   */
  public function rest_api_init() {
    //* Format the REST API output.
    \remove_filter( 'the_content', 'wpautop' );
    \add_filter( 'the_content', 'wpautop', 99 );
    \add_filter( 'the_content', [ $this->model, 'autoRow' ], 100 );
  }
}
