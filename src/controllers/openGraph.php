<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Class for controlling the openGraph
 *
 * @since 1.0
 */
class openGraph extends abstractControllers {

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'wp_head', [ $this->model, 'openGraph' ] );
    \add_filter( 'get_site_icon_url', [ $this->model, 'siteIconSize' ], 10, 3 );
    \add_filter( 'Boldface\Bootstrap\Views\openGraph\image:width', [ $this->model, 'imageWidth' ] );
    \add_filter( 'Boldface\Bootstrap\Views\openGraph\image:height', [ $this->model, 'imageHeight' ] );
  }
}
