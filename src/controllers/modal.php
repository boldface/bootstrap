<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Controllers;

/**
 * Controllers for the modal
 *
 * @since 1.0
 */
class modal extends abstractControllers {

  /**
   * @var Render priority
   *
   * @access protected
   * @since  1.0
   */
  protected $priority = 99;

  /**
   * Add actions and filters from the wp hook
   *
   * @access public
   * @since  1.0
   */
  public function wp() {
    \add_filter( 'Boldface\Bootstrap\Views\modal\trigger\attributes', [ $this->model, 'triggerAtts' ] );
    \add_filter( 'Boldface\Bootstrap\Views\modal\trigger\text', [ $this->model, 'triggerText' ] );
    \add_filter( 'Boldface\Bootstrap\Views\modal\attributes', [ $this->model, 'modalAtts' ] );
    \add_action( 'Boldface\Bootstrap\Views\loop\start', [ $this->getView(), 'trigger' ] );
  }
}
