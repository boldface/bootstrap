<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for bootstrapping the theme
 *
 * @since 1.0
 */
class theme {

  /**
   * @var Theme modules
   *
   * @access protected
   * @since  1.0
   */
  protected $modules = [];

  /**
   * Theme initiator
   *
   * @access public
   * @since  1.0
   */
  public function init() {

    //* Load the autoloader
    require __DIR__ . '/autoload.php';
    spl_autoload_register( [ new autoload(), 'load' ] );

    //* Load the updater.
    \add_action( 'init', [ new updater(), 'init' ] );

    //* Load the settings.
    $settings = new settings();
    \add_action( 'after_setup_theme', [ $settings, 'after_setup_theme' ] );
    \add_action( 'admin_init'       , [ $settings, 'admin_init' ] );
    \add_action( 'login_init'       , [ $settings, 'login_init' ] );
    \add_action( 'init'             , [ $settings, 'init' ] );
    \add_action( 'wp'               , [ $settings, 'wp' ] );

    //* Remove cruft from the header
    if( \apply_filters( 'Boldface\Bootstrap\removeFromHeader', true ) ) {
      $remove = new removeFromHeader();
      \add_action( 'init'              , [ $remove, 'init' ] );
      \add_action( 'wp_print_styles'   , [ $remove, 'wp_print_styles' ], 100 );
      \add_action( 'wp_enqueue_scripts', [ $remove, 'wp_enqueue_scripts' ] );
    }

    //* Load the REST API.
    if( \apply_filters( 'Boldface\Bootstrap\REST', true ) ) {
      $rest = new rest();
      \add_action( 'wp_enqueue_scripts', [ $rest, 'wp_enqueue_scripts' ] );
      \add_action( 'rest_api_init'     , [ $rest, 'rest_api_init' ] );
    }

    //* Set the default modules.
    $modules = [ 'header', 'navigation', 'loop', 'entry', 'footer', ];
    include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    if( \is_plugin_active( 'contact-form-7/wp-contact-form-7.php' ) )
      $modules[] = 'contactForm7';

    /**
     * Filters which modules should be included by the theme.
     *
     * Modules that are a part of the Bootstrap parent theme should have the
     * appropriate key and a value of '\Boldface\Bootstrap'. Modules that are
     * not a part of the Bootstrap parent theme need to require controller,
     * model, and view files that are named the same as the key, but in
     * separate categories.
     *
     * Modules with an integer as a key, for instance by adding to the array
     * using: `$modules[] = 'contactForm7'`, are assumed to be part of the
     * \Boldface\Bootstrap namespace with the value being the module name.
     *
     * It is recommended that child themes use the header, navigation,
     * loop, entry, and footer modules.
     *
     * Other available modules include:
     * * breadcrumbs
     * * cache
     * * contactForm7
     * * customHeader
     * * featuredImage
     * * gallery
     * * googleFonts
     * * images
     * * jumbotron
     * * modal
     * * openGraph
     * * popovers
     * * progressBar
     * * securityHeaders
     * * social
     * * widgets
     *
     * @since 1.0.0
     *
     * @param array The modules to be included by the theme.
     */
    $modules = \apply_filters( 'Boldface\Bootstrap\Controllers\modules', $modules );

    //* Load the modules.
    foreach( $modules as $moduleName => $namespace ) {
      $m = ( new module() )
        ->setModuleName( (string) $moduleName )
        ->setNamespace(  (string) $namespace  )
        ->useDefaultNamespace();

      $moduleName = $m->getModuleName();
      $this->modules[ $moduleName ] = $m->getModule();

      $modulePriority = $this->modules[ $moduleName ]->getPriority();
      if( $modulePriority >= 0 && method_exists( $this->modules[ $moduleName ]->getView(), 'html' ) )
        \add_action( 'render_html', $this->modules[ $moduleName ]->getCallback(), $modulePriority );

      foreach( [ 'after_setup_theme', 'admin_init', 'init', 'rest_api_init', 'wp', 'wp_loaded', ] as $hook )
        if( method_exists( $this->modules[ $moduleName ], $hook ) )
          \add_action( $hook, [ $this->modules[ $moduleName ], $hook ] );
    }

    /**
     * Action that fires immediately after the module hooks are added.
     *
     * @since 1.0.0
     *
     * @param array The modules that have been initalized by the theme.
     */
    \do_action( 'Boldface\Bootstrap\init', $this->modules );
    \add_filter( 'template_include', [ $this, 'renderHTML' ], 100 );
  }

  /**
   * Do the `render_html` action to render the HTML.
   *
   * This method is hooked to the `template_include` filter. By returning an
   * empty string, we're effectively short-circuiting the WordPress templating
   * system.
   *
   * @since 1.0.0
   *
   * @param string $template The template.
   *
   * @return string An empty string.
   */
  public function renderHTML( string $template ) : string {
    /**
     * Action that fires to render the HTML.
     *
     * @since 1.0.0
     */
    \do_action( 'render_html' );

    return '';
  }
}
