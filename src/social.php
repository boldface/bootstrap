<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for setting up the social media links.
 *
 * @since 1.0
 */
class social {

  /**
   * @var The social media URL.
   *
   * @access protected
   * @since  1.0
   */
  protected $url;

  /**
   * @var The social media network.
   *
   * @access protected
   * @since  1.0
   */
  protected $network;

  /**
   * @var The display name for the social media network.
   *
   * @access protected
   * @since  1.0
   */
  protected $display;

  /**
   * Return the network.
   *
   * @access public
   * @since  1.0
   *
   * @return The network.
   */
  public function getNetwork() : string {
    return $this->network ?: '';
  }

  /**
   * Set the network.
   *
   * @access public
   * @since  1.0
   *
   * @param string $url The network.
   */
  public function setNetwork( string $network ) {
    $this->network = $network;
  }

  /**
   * Return the URL.
   *
   * @access public
   * @since  1.0
   *
   * @return The URL.
   */
  public function getURL() : string {
    return $this->url ?: '';
  }

  /**
   * Set the URL.
   *
   * @access public
   * @since  1.0
   *
   * @param string $url The URL.
   */
  public function setURL( string $url ) {
    $this->url = $url;
  }

  /**
   * Return the display name.
   *
   * @access public
   * @since  1.0
   *
   * @return The display name.
   */
  public function getDisplay() : string {
    return $this->display ?: '';
  }

  /**
   * Set the display name.
   *
   * @access public
   * @since  1.0
   *
   * @param string $display The display name.
   */
  public function setDisplay( string $display ) {
    $this->display = $display;
  }
}
