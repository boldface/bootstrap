<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for controlling the REST API
 *
 * @since 1.0
 */
class rest {

  /**
   * Add actions and filters from the wp_enqueue_scripts hook
   *
   * @access public
   * @since  1.0
   */
  public function wp_enqueue_scripts() {
    \wp_enqueue_script( 'fetch', 'https://cdnjs.cloudflare.com/ajax/libs/fetch/2.0.3/fetch.min.js', [], null, true );
    \wp_enqueue_script( 'bootstrap-rest', \get_template_directory_uri() . '/assets/js/rest.js', [ 'fetch' ], null, true );
  }

  /**
   * Register the Boldface/Bootstrap request REST Route
   *
   * @access public
   * @since  1.0
   */
  public function rest_api_init() {
    \register_rest_route( 'Boldface/Bootstrap/v1', '/get/(?P<slug>(.*?))', [
      'methods'  => 'GET',
      'callback' => [ $this, 'request' ],
    ] );
  }

  /**
   * Register the Boldface/Bootstrap `request` REST Route
   *
   * @access public
   * @since  1.0
   *
   * @param \WP_REST_Request $request The WP_REST request.
   *
   * @return array An array of \WP_Post objects.
   */
  public function request( \WP_REST_Request $request ) {
    $slug = $this->sanitizeSlug( $request->get_param( 'slug' ) );

    $query = new \WP_Query( [
      'name'      => $slug,
      'post_type' => 'any',
    ] );

    return $this->sanitizePosts( $query->posts );
  }

  /**
   * Return the sanitized slug.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $slug The unsanitized slug.
   *
   * @return string The sanitized slug.
   */
  protected function sanitizeSlug( string $slug ) : string {
    $slug = \sanitize_title_for_query( $slug );
    if( $this->useFrontPage( $slug ) ) $slug = $this->frontPageSlug();
    return $slug;
  }

  /**
   * Return an array of sanitized posts.
   *
   * @access protected
   * @since  1.0
   *
   * @param array $posts An array of \WP_Post objects.
   *
   * @return array The sanitized posts.
   */
  protected function sanitizePosts( array $posts ) : array {
    array_map( function( $post ) { $this->sanitizePost( $post ); }, $posts );
    return $posts;
  }

  /**
   * Sanitize the WP_Post to include only the post_content and post_title.
   *
   * @access protected
   * @since  1.0
   *
   * @param \WP_Post $post The WP_Post object.
   */
  protected function sanitizePost( \WP_Post $post ) {
    $post->post_content = \apply_filters( 'the_content', $post->post_content );
    $keep = [
      'post_content',
      'post_title',
    ];
    foreach( $post as $key => $value ) {
      if( ! in_array( $key, $keep ) ) {
        unset( $post->$key );
      }
    }
  }

  /**
   * Return whether the slug is the front page.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $slug The slug.
   *
   * @return bool Whether the slug is the front page.
   */
  protected function useFrontPage( string $slug ) : bool {
    return 'use-front-page' === $slug;
  }

  /**
   * Return the front page slug.
   *
   * @access protected
   * @since  1.0
   *
   * @return string The front page slug.
   */
  protected function frontPageSlug() : string {
    $id = \get_option( 'page_on_front' );
    if( 0 === $id ) {
      $id = \get_option( 'page_for_posts' );
    }
    return \get_post_field( 'post_name', \get_post( $id ) );
  }
}
