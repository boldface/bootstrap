<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Bootstrap Views
 *
 * @since 1.0
 */
abstract class abstractViews {

  /**
   * Return the content wrapped in a full-width row div.
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The content to be wrapped.
   *
   * @return string The formatted content.
   */
  public function rowWrap( string $content ) : string {
    return sprintf(
      '<div class="row"><div class="%1$s">%2$s</div></div>',
      \apply_filters( 'Boldface\Bootstrap\Views\abstractViews\rowWrap', 'col-md-12' ),
      $content
    );
  }

  /**
   * Format the CSS
   *
   * @access public
   * @since  1.0
   *
   * @param string $css The CSS to be formatted
   *
   * @return string The formatted CSS
   */
  public function formatCSS( string $css ) : string {
    $css = str_replace( '  ', '', $css );
    $css = str_replace( ': ', ':', $css );
    $css = str_replace( ' {', '{', $css );
    $css = str_replace( '{ ', '{', $css );
    $css = str_replace( ' }', '}', $css );
    $css = str_replace( '} ', '}', $css );
    return str_replace( PHP_EOL, '', $css );
  }

  /**
   * Return the parsed attributes as a string
   *
   * @access protected
   * @since  1.0
   *
   * @param array $atts The HTML attributes as an array
   *
   * @return string The HTML attributes as a string
   */
  protected function parseAtts( array $atts ) : string {
    return array_reduce( array_keys( $atts ), function( $carry, $key ) use ( $atts ) {
      return sprintf( '%1$s %2$s="%3$s"', $carry, $key, htmlspecialchars( $atts[ $key ] ) );
    }, '' );
  }
}
