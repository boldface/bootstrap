<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the gallery
 *
 * @since 1.0
 */
class gallery extends abstractViews {

  /**
   * Print the gallery
   *
   * @access public
   * @since  1.0
   */
  public function gallery() : string {
    return sprintf(
      '<section id="%1$s" class="%2$s">%3$s</section>',
      \apply_filters( 'Boldface\Bootstrap\Views\gallery\id', '' ),
      \apply_filters( 'Boldface\Bootstrap\Views\gallery\class', 'gallery' ),
      \apply_filters( 'Boldface\Bootstrap\Views\gallery', '' )
    );
  }

  /**
   * Return the gallery card
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery card
   */
  public function card() : string {
    return sprintf( '
      <div class="card">
        <img class="card-img-top" src="%1$s" alt="%2$s">
        <div class="card-body">
          <h4 class="card-title">%3$s</h4>
          <p class="card-text">%4$s</p>
        </div>
      </div>',
      \apply_filters( 'Boldface\Bootstrap\Views\gallery\img\src', '' ),
      \apply_filters( 'Boldface\Bootstrap\Views\gallery\img\alt', '' ),
      \apply_filters( 'Boldface\Bootstrap\Views\gallery\title', '' ),
      \apply_filters( 'Boldface\Bootstrap\Views\gallery\text', '' )
    );
  }
}
