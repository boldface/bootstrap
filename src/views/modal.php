<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the modal
 *
 * @since 1.0
 */
class modal extends abstractViews {

  public function trigger() {
    printf(
      '<button%1$s>%2$s</button>',
      $this->parseAtts( \apply_filters( 'Boldface\Bootstrap\Views\modal\trigger\attributes', [] ) ),
      \apply_filters( 'Boldface\Bootstrap\Views\modal\trigger\text', '' )
     );
  }

  public function html() {
    print( \apply_filters( 'Boldface\Bootstrap\Views\modal', '' ) );
    ?>
    <div<?php echo $this->parseAtts( \apply_filters( 'Boldface\Bootstrap\Views\modal\attributes', [] ) ); ?>>
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Modal Title</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save Changes</button>
          </div>
        </div>
      </div>
    </div>
    <?php
  }
}
