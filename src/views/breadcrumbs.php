<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the breadcrumbs
 *
 * @since 1.0
 */
class breadcrumbs extends abstractViews {

  /**
   * Print the breadcrumb navigation
   *
   * @access public
   * @since  1.0
   */
  public function html() {
    printf(
      '<ol class="%1$s" role="navigation">%2$s</ol>',
      \apply_filters( 'Boldface\Bootstrap\Views\breadcrumbs\class', 'breadcrumb' ),
      \apply_filters( 'Boldface\Bootstrap\Views\breadcrumbs', '' )
     );
  }
}
