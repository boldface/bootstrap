<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the cache
 *
 * @since 1.0
 */
class cache extends abstractViews {
}
