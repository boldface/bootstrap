<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the contactForm7
 *
 * @since 1.0
 */
class contactForm7 extends abstractViews {

  /**
   * Print the form elements
   *
   * @access public
   * @since  1.0
   *
   * @param string $elements The form elements
   *
   * @return string The filtered form elements
   */
  public function elements( string $elements ) : string {
    return sprintf(
      '<div class="%1$s">%2$s</div>',
      \apply_filters( 'Boldface\Bootstrap\Views\contactForm7\elements\class', 'col-md-6' ),
      \apply_filters( 'Boldface\Bootstrap\Views\contactForm7\elements' , $elements )
    );
  }

  /**
   * Return the form group
   *
   * @access public
   * @since  1.0
   *
   * @param string $group The form group
   *
   * @return string The form group
   */
  public function form_group( string $label, string $input ) : string {
    return sprintf( '<div class="form-group">%1$s%2$s</div>', $label, $input );
  }

  /**
   * Return the label
   *
   * @access public
   * @since  1.0
   *
   * @param string $label The label
   * @param string $for   What the label is for
   *
   * @return string The form label
   */
  public function label( string $label, string $for ) : string {
    return sprintf( '<label for="%1$s">%2$s</label>', $for, $label );
  }

  /**
   * Return the form input
   *
   * @access public
   * @since  1.0
   *
   * @param array $args Associative array of arguments
   *
   * @return string The form input
   */
  public function input( array $args ) : string {
    $r = array_reduce( array_keys( $args ), function( $carry, $item ) use ( $args ) {
      return sprintf( '%1$s %2$s="%3$s"', $carry, $item, htmlspecialchars( $args[ $item ] ) );
    }, '' );
    return sprintf( '<input%1$s>', $r );
  }
}
