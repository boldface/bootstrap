<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the header
 *
 * @since 1.0
 */
class header extends abstractViews {

  /**
   * Print the HTML header
   *
   * @access public
   * @since  1.0
   *
   * @param array $args Arguments
   */
  public function html() {
    \do_action( 'Boldface\Bootstrap\Views\header' );
    ?><!DOCTYPE html>
<html <?php \language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php \bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo \apply_filters( 'Boldface\Bootstrap\Views\header\description', '' ); ?>">
<meta name="author" content="<?php echo \apply_filters( 'Boldface\Bootstrap\Views\header\author', '' ); ?>">
<?php \wp_head(); ?>
</head>
<body <?php \body_class(); ?>> <?php
  }

/**
 * Use custom logo on login screen.
 *
 * @access public
 * @since  1.0
 */
public function loginEnqueueScripts() {
  if( ! \has_custom_logo() ) {
    return;
  }
  $custom_logo_id = \get_theme_mod( 'custom_logo' );
  $blog_name = \get_bloginfo( 'name', 'display' );
  $image = \wp_get_attachment_image_url( $custom_logo_id, 'full', false );
  ?>
  <style>
    #login h1 a, .login h1 a {
      background-image: URL(<?php echo $image ?>);
      height: 65px;
      width: 320px;
      background-size: 320px 65px;
      background-repeat: no-repeat;
      padding-bottom: 30px;
    }
  </style><?php
  }
}
