<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the navigation
 *
 * @since 1.0
 */
class navigation extends abstractViews {

  /**
   * Print the navigation menu
   *
   * @access public
   * @since  1.0
   */
  public function html() {
    printf(
'<nav class="%1$s">
<a class="navbar-brand" href="%2$s">%3$s</a>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
%4$s
</nav>',
      \apply_filters( 'Boldface\Bootstrap\Views\navigation\class', 'navbar navbar-expand-md navbar-dark bg-dark fixed-top' ),
      \home_url('/'),
      \apply_filters( 'Boldface\Bootstrap\Views\navigation\brand', \get_bloginfo( 'name' ) ),
      \apply_filters( 'Boldface\Bootstrap\Views\navigation\menu', '' )
    );
  }
}
