<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the featuredImage image
 *
 * @since 1.0
 */
class featuredImage extends abstractViews {

  /**
   * Print the featuredImage div
   *
   * @access public
   * @since  1.0
   */
  public function defaultImage() {
    printf(
      '<div class="%1$s"><div class="%2$s" style="%3$s">%4$s</div></div>',
      \apply_filters( 'Boldface\Bootstrap\Views\featuredImage\wrapper', 'row' ),
      \apply_filters( 'Boldface\Bootstrap\Views\featuredImage\class', 'post-thumbnail' ),
      \apply_filters( 'Boldface\Bootstrap\Views\featuredImage\style', '' ),
      \apply_filters( 'Boldface\Bootstrap\Views\featuredImage', '' )
    );
  }
}
