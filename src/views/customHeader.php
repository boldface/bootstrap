<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the customHeader
 *
 * @since 1.0
 */
class customHeader extends abstractViews {

  /**
   * Return the custom header.
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The previous content.
   *
   * @return string The custom header with the previous content appended to end.
   */
   public function customHeader( string $content ) : string {
     $header = \apply_filters( 'Boldface\Bootstrap\Views\customHeader', \get_custom_header() );
     return sprintf(
       '<div class="%1$s"><img src="%2$s" height="%3$s" width="%4$s" alt="%5$s" /></div>',
       \apply_filters( 'Boldface\Bootstrap\Views\customHeader\class', 'd-flex justify-content-center' ),
       $header->url,
       isset( $header->height ) ? $header->height : '',
       isset( $header->width ) ? $header->width : '',
       \apply_filters( 'Boldface\Bootstrap\Views\customHeader\alt', '' )
     ) . $content;
  }
}
