<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the progressBar
 *
 * @since 1.2.0
 */
class progressBar extends abstractViews {

  /**
   * Print the progressBar
   *
   * @access public
   * @since  1.2.0
   */
  public function html() {
    printf(
      '<div class="%1$s"><div class="%2$s" role="progressbar" aria-valuenow="%3$s" aria-valuemin="%4$s" aria-valuemax="%5$s"></div></div>',
      \apply_filters( 'Boldface\Bootstrap\Views\progressBar\class', 'progress hide-no-js' ),
      \apply_filters( 'Boldface\Bootstrap\Views\progressBar\progress\class', 'progress-bar' ),
      \apply_filters( 'Boldface\Bootstrap\Views\progressBar\progress\valuenow', '0' ),
      \apply_filters( 'Boldface\Bootstrap\Views\progressBar\progress\valuemix', '0' ),
      \apply_filters( 'Boldface\Bootstrap\Views\progressBar\progress\valuemax', '100' )
    );
  }
}
