<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the footer
 *
 * @since 1.0
 */
class footer extends abstractViews {

  /**
   * Print the footer
   *
   * @access public
   * @since  1.0
   */
  public function html() {
    ob_start(); \wp_footer(); $footer = ob_get_clean();
    print( \apply_filters( 'Boldface\Bootstrap\Views\footer', "$footer</body></html>" ) );
    \do_action( 'Boldface\Bootstrap\Views\footer\shutdown' );
  }

  /**
   * Print the sticky footer
   *
   * @access public
   * @since  1.0
   *
   * @param string $footer The footer to append
   *
   * @return string The filtered footer
   */
  public function footer( string $footer ) : string {
    return sprintf(
      '<footer class="%1$s"><div class="%2$s">%3$s</div></footer>%4$s',
      \apply_filters( 'Boldface\Bootstrap\Views\footer\class', 'footer navbar navbar-inverse bg-light fixed-bottom' ),
      \apply_filters( 'Boldface\Bootstrap\Views\footer\wrapper\class', 'container' ),
      \apply_filters( 'Boldface\Bootstrap\Views\footer\text', '<span class="text-muted">Powered by Bootstrap</span>' ),
      $footer
     );
  }
}
