<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the entry
 *
 * @since 1.0
 */
class entry extends abstractViews {

  /**
   * Print the entry.
   *
   * @access public
   * @since  1.0
   */
  public function entry() {
    printf(
      '<article class="%1$s">%2$s</article>',
      \apply_filters( 'Boldface\Bootstrap\Views\entry\class', join( ' ', \get_post_class() ) ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry', '' )
    );
  }

  /**
   * Return the entry header.
   *
   * @access public
   * @since  1.0
   *
   * @param string $entry The unmodified entry.
   *
   * @return string The entry with the appended header.
   */
  public function entryHeader( string $entry ) : string {
    return $entry . sprintf(
      '<header class="%1$s">%2$s</header>',
      \apply_filters( 'Boldface\Bootstrap\Views\entry\header\class', 'entry-header' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\header', '' )
    );
  }

  /**
   * Return the entry heading.
   *
   * @access public
   * @since  1.0
   *
   * @param string $heading The unmodified entry.
   *
   * @return string The heading with the appended heading.
   */
  public function entryHeading( string $heading ) : string {
    return $heading . sprintf(
      '<%1$s class="%2$s">%3$s</%1$s>',
      \apply_filters( 'Boldface\Bootstrap\Views\entry\heading\element', \is_singular() ? 'h1' : 'h2' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\heading\class', 'entry-heading' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\heading', '' )
    );
  }

  /**
   * Return the entry content.
   *
   * @access public
   * @since  1.0
   *
   * @param string $entry The unmodified entry.
   *
   * @return string The entry with the appended content.
   */
  public function entryContent( string $entry ) : string {
    return $entry . sprintf(
      '<div class="%1$s">%2$s</div>',
      \apply_filters( 'Boldface\Bootstrap\Views\entry\content\class', 'entry-content' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\content', '' )
    );
  }

  /**
   * Return the entry footer.
   *
   * @access public
   * @since  1.0
   *
   * @param string $entry The unmodified entry.
   *
   * @return string The entry with the appended footer.
   */
  public function entryFooter( string $entry ) : string {
    return $entry . sprintf(
      '<footer class="%1$s">%2$s</footer>',
      \apply_filters( 'Boldface\Bootstrap\Views\entry\footer\class', 'entry-footer' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\footer', '' )
    );
  }

  /**
   * Return the comment form
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The content
   *
   * @return string The content with the comment form appended
   */
  public function comment_form( string $content ) : string {
    ob_start();
    \comment_form( [
      'format' => 'html5',
      'class_submit' => 'btn btn-primary',
    ] );
    return $content . ob_get_clean();
  }

  /**
   * Return the comments
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The content
   *
   * @return string The content with the comments appended
   */
  public function listComments( string $content ) : string {
    \comments_template();
    if( ! \have_comments() ) return $content;
    return $content . sprintf(
      '<div class="%1$s">%2$s<ul class="%3$s">%4$s</ul>%5$s</div>',
      \apply_filters( 'Boldface\Bootstrap\Views\entry\comments\class', 'comments' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\comments\header', '<h3>Comments</h3>' ),
      \apply_filters( 'Boldface\Bootstrap\Views\entry\comments\list\class', 'list-group' ),
      \wp_list_comments( [ 'page' => \get_the_ID(), 'echo' => false, ] ),
      \get_the_comments_pagination()
    );
  }

  /**
   * Return the comment form field comment
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The content
   *
   * @return string The new comment form field
   */
  public function comment_form_field_comment( $content ) {
    ob_start();?>
    <div class="form-group">
      <label for="comment">Comment</label>
      <textarea class="form-control" id="commment" name="comment" cols="45" rows="8" maxlength="65525" aria-required="true" required="required"></textarea>
    </div>
    <?php return ob_get_clean();
  }
}
