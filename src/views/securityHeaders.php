<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the securityHeaders.
 *
 * @since 1.0
 */
class securityHeaders extends abstractViews {
}
