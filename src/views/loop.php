<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the loop
 *
 * @since 1.0
 */
class loop extends abstractViews {

  /**
   * Print the HTML for the loop
   *
   * @access public
   * @since  1.0
   */
  public function html() {
    printf(
      '<main class="%1$s">%2$s</main>',
      \apply_filters( 'Boldface\Bootstrap\Views\loop\class', 'container' ),
      \apply_filters( 'Boldface\Bootstrap\Views\loop', '' )
    );
  }

  /**
   * Print the page title
   *
   * @access public
   * @since  1.0
   *
   * @param string $title The title to be filtered
   */
  public function pageTitle() {
    printf(
      '<header class="%1$s">%2$s</header>',
      \apply_filters( 'Boldface\Bootstrap\Views\loop\title\class', 'page-title' ),
      \apply_filters( 'Boldface\Bootstrap\Views\loop\title', '' )
    );
  }

  /**
   * Print the page body
   *
   * @access public
   * @since  1.0
   *
   * @param string $title The body to be filtered. Unused.
   */
  public function pageBody() {
    printf(
      '<div class="%1$s">%2$s</div>',
      \apply_filters( 'Boldface\Bootstrap\Views\loop\body\class', 'error-body' ),
      \apply_filters( 'Boldface\Bootstrap\Views\loop\body', '' )
    );
  }
}
