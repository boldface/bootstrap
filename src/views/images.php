<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the images
 *
 * @since 1.0
 */
class images extends abstractViews {

  /**
   * Return the figure with it's caption
   *
   * @access public
   * @since  1.0
   *
   * @param string $id      The figure ID
   * @param string $class   The figure class
   * @param string $content The figure content
   * @param string $caption The figure caption
   *
   * @return string The processed [caption] shortcode
   */
  public function caption( string $id, string $class, string $content, string $caption ) {
    return sprintf(
      '<figure id="%1$s" class="%2$s">%3$s<figcaption>%4$s</figcaption></figure>',
      $id,
      $class,
      $content,
      $caption
    );
  }
}
