<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the jumbotron
 *
 * @since 1.0
 */
class jumbotron extends abstractViews {

  /**
   * Print the jumbotron HTML.
   *
   * @access public
   * @since  1.0
   */
  public function html() {
    printf(
      '<div class="%1$s">%2$s</div>',
      \apply_filters( 'Boldface\Bootstrap\Views\jumbotron\class', 'jumbotron' ),
      \apply_filters( 'Boldface\Bootstrap\Views\jumbotron', '' )
     );
  }

  /**
   * Return the jumbotron header.
   *
   * @access public
   * @since  1.0
   *
   * @return string The jumbotron header.
   */
  public function jumbotronHeader( string $jumbotron ) : string {
    return sprintf(
      '<h1 class="%1$s">%2$s</h1>',
      \apply_filters( 'Boldface\Bootstrap\Views\jumbotron\header\class', 'display-3' ),
      \apply_filters( 'Boldface\Bootstrap\Views\jumbotron\header', \get_the_title() )
    );
  }
}
