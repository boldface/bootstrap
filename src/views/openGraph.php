<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the openGraph
 *
 * @since 1.0
 */
class openGraph extends abstractViews {

  /**
   * Print the Open Graph title
   *
   * @access public
   * @since  1.0
   */
  public function title() {
    $this->printMetaProperty( __FUNCTION__, \get_the_title() );
  }

  /**
   * Print the Open Graph type
   *
   * @access public
   * @since  1.0
   */
  public function type() {
    $this->printMetaProperty( __FUNCTION__, 'article' );
  }

  /**
   * Print the Open Graph image
   *
   * @access public
   * @since  1.0
   */
  public function image() {
    $image_url = \get_site_icon_url() ?: \get_template_directory_uri() . '/screenshot.png';
    $this->printMetaProperty( __FUNCTION__, $image_url );
  }

  /**
   * Print the Open Graph image:type
   *
   * @access public
   * @since  1.0
   */
  public function imageType() {
    $this->printMetaProperty( 'image:type', '' );
  }

  /**
   * Print the Open Graph image:width
   *
   * @access public
   * @since  1.0
   */
  public function imageWidth() {
    $this->printMetaProperty( 'image:width', '' );
  }

  /**
   * Print the Open Graph image:height
   *
   * @access public
   * @since  1.0
   */
  public function imageHeight() {
    $this->printMetaProperty( 'image:height', '' );
  }

  /**
   * Print the Open Graph url
   *
   * @access public
   * @since  1.0
   */
  public function url() {
    $this->printMetaProperty( __FUNCTION__, \get_the_permalink() ?: '' );
  }

  /**
   * Print the Open Graph description
   *
   * @access public
   * @since  1.0
   */
  public function description() {
    $this->printMetaProperty( __FUNCTION__, \get_the_excerpt() );
  }

  /**
   * Print the Open Graph locale
   *
   * @access public
   * @since  1.0
   */
  public function locale() {
    $this->printMetaProperty( __FUNCTION__, \get_bloginfo( 'language' ) );
  }

  /**
   * Print the Open Graph meta property
   *
   * @access protected
   * @since  1.0
   *
   * @param string $property The Open Graph property
   * @param string $default  The default value for the Open Graph property
   */
  protected function printMetaProperty( string $property, string $default ) {
    $filter = "Boldface\Bootstrap\Views\openGraph\\$property";
    $content =  \apply_filters( $filter, $default );

    if( ! $content ) return;

    printf(
      '<meta property="og:%1$s" content="%2$s" />',
      $property,
      $content
    );
  }
}
