<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the googleFonts
 *
 * @since 1.0
 */
class googleFonts extends abstractViews {

  /**
   * @var array Google Fonts
   *
   * @access protected
   * @since  1.0
   */
  protected $fonts = [];

  /**
   * Set the fonts array.
   *
   * @access public
   * @since  1.0
   *
   * @param array $fonts Array of fonts to set.
   */
  public function setFonts( array $fonts ) {
    $this->fonts = array_map( function( $font ) {
      return str_replace( ' ', '+', $font );
    }, $fonts );
  }

  /**
   * Return whether the fonts have been set.
   *
   * @access public
   * @since  1.0
   *
   * @return bool Whether the fonts have been set.
   */
  public function hasFonts() : bool {
    return [] !== $this->fonts;
  }

  /**
   * Link the Google Fonts.
   *
   * @access public
   * @since  1.0
   */
  public function link() {
    printf(
      '<link href="//fonts.googleapis.com/css?family=%1$s" rel="stylesheet">',
      implode( '%7C', $this->fonts )
    );
  }
}
