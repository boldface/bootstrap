<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the widgets
 *
 * @since 1.0
 */
class widgets extends abstractViews {

  /**
   * Print the sidebar.
   *
   * @access public
   * @since  1.0
   *
   * @param string $id The id of the sidebar.
   */
  public function widget( string $id ) {
    if( ! \is_active_sidebar( $id ) ) return;

    /**
     * Filters whether to short-circuit displaying the widget.
     *
     * @since 1.0.0
     *
     * @param string The ID of the widget.
     */
    if( \apply_filters( 'Boldface\Bootstrap\Views\widgets\\' . $id, false ) ) return;

    printf(
      '<aside id="%1$s" class="%2$s" role="%3$s">%4$s</aside>',
      \apply_filters( 'Boldface\Bootstrap\Views\widgets\id', $id ),
      \apply_filters( 'Boldface\Bootstrap\Views\widgets\class', 'sidebar widget-area', $id ),
      \apply_filters( 'Boldface\Bootstrap\Views\widgets\role', 'complementary', $id ),
      $this->getSidebar( $id )
    );
  }

  /**
   * Return the dynamic sidebar.
   *
   * @access private
   * @since  1.0
   *
   * @param string $id The id of the sidebar.
   */
  private function getSidebar( string $id ) : string {
    ob_start(); \dynamic_sidebar( $id ); return ob_get_clean();
  }
}
