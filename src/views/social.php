<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the social.
 *
 * @since 1.0
 */
class social extends abstractViews {

  /**
   * @var Which social networks to use.
   *
   * @access protected
   * @since  1.0
   */
  protected $social = [];

  /**
   * @var The author.
   *
   * @access protected
   * @since  1.0
   */
  protected $author;

  /**
   * Set which social networks to use.
   *
   * @access public
   * @since  1.0
   *
   * @param array $social Which social networks to use.
   */
  public function setSocial( array $social ) {
    $this->social = $social;
    if( \is_admin() ) return;

    global $author;
    $this->author = $author;
    $this->processSocial();
  }

  /**
   * Print the social.
   *
   * @access public
   * @since  1.0
   */
  public function html() {
    print( $this->getHtml() );
  }

  /**
   * Return the social HTML.
   *
   * @access public
   * @since  1.0
   *
   * @return string The social HTML.
   */
  public function getHtml() : string {
    return sprintf(
      '<aside class="%1$s">%2$s</aside>',
      \apply_filters( 'Boldface\Bootstrap\Views\social\class', 'container social' ),
      \apply_filters( 'Boldface\Bootstrap\Views\social', '' )
    );
  }

  /**
   * Print the social media links.
   *
   * @access public
   * @since  1.0
   *
   * @param string $html The html.
   */
  public function social( string $html ) : string {
    foreach( $this->social as $key => $social ) {
      if( '' === $social->getURL() ) continue;
      $html .= sprintf(
        '<div class="%1$s"><a href="%2$s" class="%3$s"><img class="img img-fluid" src="%4$s"/></a></div>',
        \apply_filters( 'Boldface\Bootstrap\Views\social\wrapper\class', 'col-md-1', $this->count() ),
        $social->getURL(),
        $key,
        \get_template_directory_uri() . sprintf( '/assets/images/%1$s.svg', $key )
      );
    }
    if( '' === $html ) return '';

    return sprintf( '<div class="row justify-content-center">%1$s</div>', $html );
  }

  /**
   * Print the social media options on the user profile.
   *
   * @access public
   * @since  1.0
   *
   * @param \WP_User $user The WP_User object.
   */
  public function show_user_profile( \WP_User $user ) {
    $this->author = $user->ID;
    $this->processSocial();
    if( [] === $this->social || ! is_array( $this->social ) ) return;
    $inner = '';
      foreach( $this->social as $key => $social ) {
        $meta = "bootstrap_{$social->getNetwork()}_url";
        $initial = '';
        if( is_object( $social ) && is_a( $social, '\Boldface\Bootstrap\social' ) ) {
          $initial = \esc_attr( $social->getURL() );
        }
        $inner .= sprintf(
          '<tr><th><label for="%1$s">%2$s:</lable></th><td><input type="text" name="%1$s" id="%1$s" value="%3$s" class="regular-text"></td></tr>',
          $meta,
          $social->getDisplay(),
          $initial
        );
      }
    printf( '<table class="form-table"><h3>Social Media</h3>%1$s</table>', $inner );
  }

  /**
   * Process the social media array. Remove unused keys and replace values with
   * the approtriate social object.
   *
   * @access protected
   * @since  1.0
   */
  protected function processSocial() {
    foreach( $this->social as $key => $value ) {
      $meta = "bootstrap_{$key}_url";
      $social = \get_user_meta( $this->author, $meta, true );

      if(
        ! is_object( $social ) ||
        ! is_a( $social, '\Boldface\Bootstrap\social' ) ||
        '' === $url = $social->getURL()
      ) {
        $social = new \Boldface\Bootstrap\social();
        $social->setNetwork( $key );
        $social->setDisplay( $value );
        $this->social[ $key ] = $social;
        continue;
      }

      $social->setDisplay( $value );
      $this->social[ $key ] = $social;
    }
  }

  /**
   * Return the number of the used social media.
   *
   * @access protected
   * @since  1.0
   *
   * @return int The number of used social media.
   */
  protected function count() {
    $count = 0;
    foreach( $this->social as $key => $social )
      if( '' !== $social->getURL() ) $count++;

    return $count;
  }
}
