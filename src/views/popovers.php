<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Views;

/**
 * Views for the popovers
 *
 * @since 1.0
 */
class popovers extends abstractViews {
}
