<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Social model
 *
 * @since 1.0
 */
class social extends abstractModels {

  /**
   * @var Which social networks to use.
   *
   * @access protected
   * @since  1.0
   */
  protected $social = [];

  /**
   * Return the social networks to use.
   *
   * @access public
   * @since  1.0
   *
   * @return array An array of social networks to sue.
   */
  public function getSocial() : array {
    return $this->social;
  }

  /**
   * Set up the social media links.
   *
   * @access public
   * @since  1.0
   */
  public function setUpSocial() {

    /**
     * Filters which social networks to use.
     *
     * @since 1.0.0
     *
     * @param array The modules to be included by the theme.
     */
    $this->social = \apply_filters( 'Boldface\Bootstrap\Models\social', [
      'facebook'  => 'Facebook',
      'twitter'   => 'Twitter',
      'linkedin'  => 'LinkedIn',
      'instagram' => 'Instagram',
      'website'   => 'Website',
    ] );
    $this->getView()->setSocial( $this->social );
  }

  /**
   * Update the user social.
   *
   * @access public
   * @since  1.0
   *
   * @param int $user_id The user ID.
   */
  public function profile_update( int $user_id ) {
    if( ! \current_user_can( 'edit_user', $user_id ) ) return false;

    foreach( $this->social as $key => $value ) {
      $meta = "bootstrap_{$key}_url";

      if( ! isset( $_POST[ $meta ] ) ) continue;

      $url = \esc_url_raw( $_POST[ $meta ] );
      $url = str_replace( 'http://', 'https://', $url );

      $social = new \Boldface\Bootstrap\social();
      $social->setNetwork( $key );
      $social->setURL( $url );
      $social->setDisplay( $value );

      \update_user_meta( $user_id, $meta, $social );
    }
  }
}
