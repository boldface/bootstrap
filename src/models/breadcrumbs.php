<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Breadcrumbs model
 *
 * @since 1.0
 */
class breadcrumbs extends abstractModels {

  /**
   * Return the breadcrumbs
   *
   * @access public
   * @since  1.0
   *
   * @return string The breadcrumbs
   */
  public function breadcrumbs( string $breadcrumbs ) : string {
    if( ! \is_page() ) return '';
    $post_id = \get_post()->ID;
    $a = \get_post_ancestors( $post_id );

    if( ! \is_front_page() )
      $a[] = $post_id;

    return array_reduce( $a, function( $carry, $item ) {
      return $carry . $this->breadcrumbItem( $item );
    }, $this->breadcrumbHomeItem() );
  }

  /**
   * Return the breadcrumb item.
   *
   * @access protected
   * @since  1.0
   *
   * @param int $post_id The post ID.
   *
   * @return string The breadcrumb item.
   */
  protected function breadcrumbItem( int $post_id ) {
    return ( \get_post()->ID === $post_id ) ? $this->breadcrumbActive( $post_id ) : $this->breadcrumbInactive( $post_id );
  }

  /**
   * Return the inactive breadcrumb item.
   *
   * @access protected
   * @since  1.0
   *
   * @param int $post_id The post ID.
   *
   * @return string The inactive breadcrumb item.
   */
  protected function breadcrumbInactive( int $post_id ) : string {
    return $this->breadcrumbListItem( 'breadcrumb-item', $this->breadcrumbInactiveTitle( $post_id ) );
  }

  /**
   * Return the inactive breadcrumb title.
   *
   * @access protected
   * @since  1.0
   *
   * @param int $post_id The post ID.
   *
   * @return string The inactive breadcrumb title.
   */
  protected function breadcrumbInactiveTitle( int $post_id ) : string {
    return sprintf( '<a href="%1$s">%2$s</a>', \get_the_permalink( $post_id ), \get_the_title( $post_id ) );
  }

  /**
   * Return the active breadcrumb title.
   *
   * @access protected
   * @since  1.0
   *
   * @param int $post_id The post ID.
   *
   * @return string The active breadcrumb title.
   */
  protected function breadcrumbActive( int $post_id ) : string {
    return $this->breadcrumbListItem( 'breadcrumb-item active', \get_the_title( $post_id ) );
  }

  /**
   * Return the breadcrumb list item.
   *
   * @access protected
   * @since  1.0
   *
   * @param int $post_id The post ID.
   *
   * @return string The breadcrumb list item.
   */
  protected function breadcrumbListItem( string $class, string $listItem ) :string {
    return sprintf( '<li class="%1$s">%2$s</li>', $class, $listItem );
  }

  /**
   * Return the breadcrumb home item.
   *
   * @access protected
   * @since  1.0
   *
   * @param int $post_id The post ID.
   *
   * @return string The breadcrumb home item.
   */
  protected function breadcrumbHomeItem() : string {
    return \is_front_page() ? $this->breadcrumbActive( \get_post()->ID ) :
      sprintf( '<li class="breadcrumb-item"><a href="%1$s">%2$s</a></li>', \home_url( '/' ), \get_bloginfo( 'name' ) );
  }
}
