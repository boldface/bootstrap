<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * images model
 *
 * @since 1.0
 */
class images extends abstractModels {

  /**
   * Filter the image class when inserted into the WordPress post
   *
   * @access public
   * @since  1.0
   *
   * @param string $class The class to be filtered
   * @param int    $id    The post ID
   * @param string $align The image alignment
   * @param mixed  $size  Size of image. Image size or array of width and height values (in that order)
   *
   * @return string The filtered image class
   */
  public function get_image_tag_class( string $class, int $id, string $align, $size ) : string {
    switch ( $align ) {
      case 'left':
        return 'img img-fluid pr-3 float-left';
        break;
      case 'center':
        return 'img img-fluid mx-auto d-block';
        break;
      case 'right':
        return 'img img-fluid pl-3 float-right';
        break;
      default:
        return 'img img-fluid';
        break;
    }
  }

  /**
   * Process and return the [caption] shortcode
   *
   * @access public
   * @since  1.0
   *
   * @param string $html    The HTML to be filtered
   * @param array  $atts    The shortcode attributes
   * @param string $content The shortcode content
   *
   * @return string The [caption] shortcode
   */
  public function img_caption_shortcode( string $html, array $atts, string $content = '' ) : string {
    $atts = \shortcode_atts( [
      'id'      => '',
      'align'   => '',
      'caption' => '',
      'class'   => ''
    ], $atts, 'caption' );

    switch ( $atts[ 'align' ] ?: 'alignnone' ) {
      case 'alignleft':
        $atts[ 'class' ] .= ' float-left';
        break;
      case 'aligncenter':
      $atts[ 'class' ] .= ' mx-auto d-block';
        break;
      case 'alignright':
        $atts[ 'class' ] .= ' float-right';
        break;
      default:
        break;
    }

    return $this->view->caption(
      trim( $atts[ 'id' ] ),
      trim( $atts[ 'class' ] ),
      \do_shortcode( $content ), $atts[ 'caption' ]
    );
  }

  /**
   * Return the filtered attachment image attributes
   *
   * @access public
   * @since  1.0
   *
   * @param array    $attr       An array of image attributes
   * @param \WP_Post $attachment The \WP_Post object for the attachment image
   * @param mixed    $size       Size of image. Image size or array of width and height values (in that order)
   *
   * @return array The filtered attachment image attributes
   */
  public function wp_get_attachment_image_attributes( array $attr, \WP_Post $attachment, $size ) : array {
    $attr[ 'class' ] = is_string( $attr[ 'class' ] ) ? $attr[ 'class' ] . ' img img-fluid' : 'img img-fluid';
    return $attr;
  }
}
