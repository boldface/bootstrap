<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Models for the featuredImage image
 *
 * @since 1.0
 */
class featuredImage extends abstractModels {

  /**
   * @var WordPress attachment ID for the featuredImage image
   *
   * @access protected
   * @since  1.0
   */
  protected $id;

  /**
   * @var Size of the featuredImage image. Array where first element is width, second is height.
   *
   * @access protected
   * @since  1.0
   */
  protected $size;

  /**
   * @var Aspect ration of the featuredImage image. String. Either landscape or portrait.
   *
   * @access protected
   * @since  1.0
   */
  protected $aspectRatio;

  /**
   * Return whether the post has a featuredImage image
   *
   * @access public
   * @since  1.0
   *
   * @return bool Whether the post has a featuredImage image
   */
  public function hasfeaturedImage() : bool {
    if( ! \has_post_thumbnail() ) return false;
    $this->setVars();
    return true;
  }

  /**
   * Set the variables associated with the featuredImage image
   *
   * @access protected
   * @since  1.0
   */
  protected function setVars() {
    $this->id = \get_post_thumbnail_id();
    $src = \wp_get_attachment_image_src( $this->id, 'full' );
    $this->src = $src[0];
    $this->size = [ $src[1], $src[2] ];
    $this->setAspectRatio( $src[1] > $src[2] ? 'landscape' : 'portrait' );
  }

  /**
   * Return the modified post class
   *
   * @access public
   * @since  1.0
   *
   * @param array $class The unfiltered post class
   *
   * @return array The modified post class
   */
  public function postClass( array $class ) : array {
    $class[] = $this->getAspectRatio();
    return $class;
  }

  /**
   * Return the featured image wrapper class.
   *
   * @access public
   * @since  1.0
   *
   * @param string $class The original featured image wrapper class.
   *
   * @return string The featured image wrapper class.
   */
  public function wrapperClass( string $class ) : string {
    return 'landscape' === $this->getAspectRatio() ? 'row' : 'float-left';
  }

  /**
   * Return the featured image class.
   *
   * @access public
   * @since  1.0
   *
   * @param string $class The original featured image class.
   *
   * @return string The featured image class.
   */
  public function featuredImageClass( string $class ) : string {
    return 'landscape' === $this->getAspectRatio() ? trim( $class . ' col-md-12' ) : $class;
  }

  /**
   * Return the featured image aspect ratio.
   *
   * @access public
   * @since  1.0
   *
 * @return string The featured image class.
   */
  public function getAspectRatio() : string {
    return ( in_array( $this->aspectRatio, [ 'landscape', 'portrait' ] ) ) ? $this->aspectRatio : '';
  }

  /**
   * Set the featured image aspect ratio.
   *
   * @access public
   * @since  1.0
   *
   * @param string $aspectRatio The featured image aspect ratio. Must be either 'landscape' or 'portrait'. Default 'landscape'.
   */
  public function setAspectRatio( string $aspectRatio ) {
    $this->aspectRatio = ( in_array( $aspectRatio, [ 'landscape', 'portrait' ] ) ) ? $aspectRatio : 'landscape';
  }
}
