<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Widgets model
 *
 * @since 1.0
 */
class widgets extends abstractModels {

  /**
   * @var Which sidebars to use.
   *
   * @access protected
   * @since  1.0
   */
  protected $sidebars = [];

  /**
   * Process the callbacks.
   *
   * @access private
   * @since  1.0
   */
  public function processCallbacks() {
    foreach( $this->sidebars as $sidebar ) {
      if( ! isset( $sidebar[ 'hook' ] ) ) continue;
      $view = $this->getView();
      \add_action( $sidebar[ 'hook' ], function() use ( $view, $sidebar ) {
        $view->widget( $sidebar[ 'id' ] );
      } );
    }
  }

  /**
   * Register the sidebars.
   *
   * @access public
   * @since  1.0
   */
  public function registerSidebars() {
    /**
     * Filters which sidebars to register.
     *
     * @param array A numbered array of sidebars to register.
     */
    $this->sidebars = \apply_filters( 'Boldface\Bootstrap\Models\widgets\sidebars', $this->sidebars );
    array_map( 'register_sidebar', $this->sidebars );
  }
}
