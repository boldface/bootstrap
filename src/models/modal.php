<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Modal model
 *
 * @since 1.0
 */
class modal extends abstractModels {

  public function triggerAtts( array $atts ) : array {
    return [
      'type' => 'button',
      'class' => 'btn btn-primary',
      'data-toggle' => 'modal',
      'data-target' => '#exampleModal',
    ];
  }

  public function triggerText( string $text ) : string {
    return 'Launch demo modal';
  }

  public function modalAtts( array $atts ) : array {
    return [
      'class'           => 'modal fade',
      'id'              => 'exampleModal',
      'tabindex'        => '-1',
      'role'            => 'dialog',
      'aria-labelledby' => 'exampleModalLabel',
      'aria-hidden'     => 'true',
    ];
  }

  public function modalHeader() {

  }

  public function modalBody( string $body ) : string {

  }

  public function modalFooter() {

  }
}
