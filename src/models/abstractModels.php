<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Abstract model
 *
 * @since 1.0
 */
abstract class abstractModels {

  /**
   * @var Model view
   *
   * @access public
   * @since  1.0
   */
  protected $view;

  /**
   * Class constructor
   *
   * @access public
   * @since  1.0
   *
   * @param $view The view
   */
  public function __construct( $view ) {
    $class = str_replace( 'Models', 'Views', __CLASS__ );
    $this->view = $view instanceof $class ? $view : null;
  }

  /**
  * Return the view.
  *
  * @access public
  * @since  1.0
  *
  * @return The view.
  */
  public function getView() {
    return $this->view;
  }
}
