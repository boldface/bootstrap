<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * progressBar model
 *
 * @since 1.2.0
 */
class progressBar extends abstractModels {

  /**
   * Enqueue the progressBar javascript.
   *
   * @access public
   * @since  1.2.0
   */
  public function enqueueScripts() {
    \wp_enqueue_script( 'progressBar', \esc_url( \get_template_directory_uri() . '/assets/js/progressBar.js' ), [ 'bootstrap' ] );
  }
}
