<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Header model
 *
 * @since 1.0
 */
class header extends abstractModels {

  /**
   * Remove body classes that contain the phrases 'id' or 'template'
   *
   * @access public
   * @since  1.0
   *
   * @param array $classes The classes to filter
   *
   * @return array The filtered classes
   */
  public function bodyClass( array $classes ) : array {
    return array_filter( $classes, function( $class ) {
      return ! ( false !== mb_strpos( $class, 'id' ) || false !== mb_strpos( $class, 'template' ) );
    });
    return $classes;
  }

  /**
   * Return the singular page meta description
   *
   * @access public
   * @since 1.0
   *
   * @param string $description The old description. Unused.
   *
   * @return string The meta description
   */
  public function singularDescription( string $description ) : string {
    $post = \get_post();
    $excerpt = $post->post_excerpt;
    if( '' === $excerpt ) {
      $excerpt = strip_tags( $post->post_content );
      $excerpt = preg_replace( '/\s+/', ' ', $excerpt );
      $excerpt = mb_substr( $excerpt, 0, 255 );
    }
    $excerpt = str_replace( '"', "'", $excerpt );
    return str_replace( PHP_EOL, ' ', \strip_shortcodes( $excerpt ) );
  }

  /**
   * Return the singular description, if it exists. Fallback to blog page meta description.
   *
   * @access public
   * @since 1.0
   *
   * @param string $description The old description. Unused.
   *
   * @return string The blog meta description
   */
  public function blogDescription( string $description ) : string {
    return '' !== ( $description = $this->singularDescription( $description ) ) ? $description : (string) \get_bloginfo( 'description' );
  }

  /**
   * Return the meta description for category pages.
   *
   * @access public
   * @since 1.0
   *
   * @param string $description The old description. Unused.
   *
   * @return string The category description.
   */
  public function categoryDescription( string $description ) : string {
    $category = array_reduce( \get_the_category(), function( $carry, $item ) {
      return '' === $carry ? strtolower( $item->name ) : $carry . ', ' . strtolower( $item->name );
    }, '' );
    return sprintf( 'Learn about %1$s on %2$s.', $category, \get_bloginfo( 'name' ) );
  }

  /**
   * Return the meta description for tag pages.
   *
   * @access public
   * @since 1.0
   *
   * @param string $description The old description. Unused.
   *
   * @return string The tag description.
   */
  public function tagDescription( string $description ) : string {
    return sprintf( 'Learn about %1$s on %2$s.', \single_tag_title( '', false ), \get_bloginfo( 'name' ) );
  }

  /**
   * Return the meta description for tag pages.
   *
   * @access public
   * @since 1.0
   *
   * @param string $description The old description. Unused.
   *
   * @return string The tag description.
   */
  public function errorDescription( string $description ) : string {
    return '404 Not found.';
  }

  /**
   * Return the default page author
   *
   * @access public
   * @since 1.0
   *
   * @return string The default page author
   */
  public function defaultAuthor() : string {
    return \get_bloginfo( 'name' );
  }

  /**
   * Return the singular page author
   *
   * @access public
   * @since 1.0
   *
   * @return string The page author
   */
  public function singularAuthor() : string {
    return ( $id = \get_the_ID() ) ? \get_userdata( \get_post_field( 'post_author', \get_the_ID() ) )->display_name : '';
  }
}
