<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Model of the securityHeaders
 *
 * @since 1.0
 */
class securityHeaders extends abstractModels {

  /**
   * @var Content security policies.
   *
   * @access protected
   * @since  1.0
   */
  protected $contentSecurityPolicies = [
    'connect-src',
    'default-src',
    'font-src',
    'frame-src',
    'img-src',
    'manifest-src',
    'media-src',
    'object-src',
    'script-src',
    'style-src',
    'worker-src',
    'base-uri',
    'plugin-types',
    'sandbox',
    'form-action',
    'frame-ancestors',
    'block-all-mixed-content',
    'require-sri-for',
    'upgrade-insecure-requests',
  ];

  /**
   * Send the Content-Security-Policy header.
   *
   * @access public
   * @since  1.0
   */
  public function contentSecurityPolicy() {
    $policy = $this->getContentSecurityPolicy();
    header( 'Content-Security-Policy: ' . $policy );
  }

  /**
   * Return the content security policy.
   *
   * @access public
   * @since  1.0
   *
   * @return string The content security policy.
   */
  protected function getContentSecurityPolicy() : string {
    $content = array_reduce( $this->contentSecurityPolicies, function( string $carry, string $item ) : string {
      $policy = \apply_filters( 'Boldface\Bootstrap\Models\securityHeaders\CSP\\' . $item, [] );
      if( [] === $policy ) return $carry;
      return sprintf( '%1$s %2$s %3$s;', $carry, $item, implode( ' ', $policy ) );
    }, '' );
    return trim( $content );
  }

  /**
   * Send the strict-transport-security header.
   *
   * @access public
   * @since  1.0
   */
  public function strictTransportSecurity() {
    header( 'Strict-Transport-Security: max-age=31536000; includeSubDomains' );
  }

  /**
   * Send the X-Frame-Options header.
   *
   * @access public
   * @since  1.0
   */
  public function xFrameOptions() {
    $policy = \apply_filters( 'Boldface\Bootstrap\Models\securityHeaders\xFrameOptions', 'SAMEORIGIN' );
    $policy = $this->sanitizeXFrameOptions( $policy );
    header( "X-Frame-Options: $policy" );
  }

  /**
   * Sanitize the X-Frame-Options header.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $policy The unsanitized header.
   *
   * @return string The sanitized X-Frame-Options header.
   */
  protected function sanitizeXFrameOptions( string $policy ) : string {
    $firstWord = explode( ' ', $policy )[0];
    return in_array( $firstWord, [ 'ALLOW-FROM', 'SAMEORIGIN', 'DENY' ] ) ? $policy : 'DENY';
  }

  /**
   * Send the X-XSS-Protection header.
   *
   * @access public
   * @since  1.0
   */
  public function xssProtection() {
    $policy = \apply_filters( 'Boldface\Bootstrap\Models\securityHeaders\xssProtection', '1; mode=block' );
    $policy = $this->sanitizeXssProtection( $policy );
    header( "X-XSS-Protection: $policy" );
  }

  /**
   * Sanitize the XSS protection header.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $policy The unsanitized header.
   *
   * @return string The sanitized XSS protection header.
   */
  protected function sanitizeXssProtection( string $policy ) : string {
    $policies = [
      '0',
      '1',
      '1; mode=block',
    ];
    return in_array( $policy, $policies ) ? $policy : '1; mode=block';
  }

  /**
   * Send the X-Content-Type-Options header.
   *
   * @access public
   * @since  1.0
   */
  public function xContentTypeOptions() {
    header( 'X-Content-Type-Options: nosniff' );
  }

  /**
   * Send the Referrer-Policy header.
   *
   * @access public
   * @since  1.0
   */
  public function referrerPolicy() {
    $policy = \apply_filters( 'Boldface\Bootstrap\Models\securityHeaders\referrerPolicy', 'strict-origin' );
    $policy = $this->sanitizeReferrerPolicy( $policy );
    header( "Referrer-Policy: $policy" );
  }

  /**
   * Sanitize the referrer policy header.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $policy The unsanitized header.
   *
   * @return string The sanitized referrer policy header.
   */
  protected function sanitizeReferrerPolicy( string $policy ) : string {
    $policies = [
      '',
      'no-referrer',
      'no-referrer-when-downgrade',
      'same-origin',
      'strict-origin',
      'origin-when-cross-origin',
      'strict-origin-when-cross-origin',
      'unsafe-url',
    ];
    return in_array( $policy, $policies ) ? $policy : 'strict-origin';
  }

  /**
   * Return 'self' for the content security policy.
   *
   * @access public
   * @since  1.0
   *
   * @param array $csp Previous content security policy. Unused.
   *
   * @return array New content security policy.
   */
  public function self( array $csp ) : array {
    return [ "'self'" ];
  }

  /**
   * Filter the script-src content security policy.
   *
   * @access public
   * @since  1.0
   *
   * @param array $csp Previous content security policy. Unused.
   *
   * @return array New content security policy.
   */
  public function script( array $csp ) : array {
    return [
      "'self'",
      'https://cdnjs.cloudflare.com',
      'https://maxcdn.bootstrapcdn.com',
      'https://code.jquery.com',
    ];
  }

  /**
   * Filter the style-src content security policy.
   *
   * @access public
   * @since  1.0
   *
   * @param array $csp Previous content security policy. Unused.
   *
   * @return array New content security policy.
   */
  public function style( array $csp ) : array {
    return [
      "'self'",
      'https://fonts.googleapis.com',
      'https://maxcdn.bootstrapcdn.com',
    ];
  }

  /**
   * Filter the font-src content security policy.
   *
   * @access public
   * @since  1.0
   *
   * @param array $csp Previous content security policy. Unused.
   *
   * @return array New content security policy.
   */
  public function font( array $csp ) : array {
    return [
      "'self'",
      'https://fonts.gstatic.com',
    ];
  }
}
