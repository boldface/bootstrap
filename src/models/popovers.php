<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Model for the popovers
 *
 * @since 1.0
 */
class popovers extends abstractModels {

  /**
   * Add the popover JS as an inline script
   *
   * @access public
   * @since  1.0
   */
  public function addInlineScript() {
    \wp_add_inline_script( 'bootstrap', sprintf(
      'window.addEventListener(\'DOMContentLoaded\',function(){jQuery(function(){jQuery(\'%1$s\').popover(%2$s);});});',
      \apply_filters( 'Boldface\Bootstrap\Views\popovers\select', '[data-toggle="popover"]' ),
      \apply_filters( 'Boldface\Bootstrap\Views\popovers\options', '' )
    ) );
  }
}
