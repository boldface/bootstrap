<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Models for the cache
 *
 * @since 1.0
 */
class cache extends abstractModels {

  /**
   * @var Transient expiration
   *
   * @access protected
   * @since  1.0
   */
  protected $expiration = DAY_IN_SECONDS;

  /**
   * @var Transient name
   *
   * @access protected
   * @since  1.0
   */
  protected $transient_name = '';

  /**
   * @var Transient value
   *
   * @access protected
   * @since  1.0
   */
  protected $transient;

  /**
   * If the page is cached, display it.
   *
   * @access public
   * @since  1.0
   */
  public function doCache() {
    $this->setTransientName();
    if( $this->hasTransient() ) {
      echo $this->getTransient();
      exit;
    }
  }

  /**
   * Start the output buffer.
   *
   * @access public
   * @since  1.0
   */
  public function ob_start() {
    ob_start();
  }

  /**
   * Echo the output buffer and set transient value.
   *
   * @access public
   * @since  1.0
   */
  public function ob_get_clean() {
    $html = ob_get_clean();
    echo $html;
    \set_transient( $this->transient_name, $html, $this->expiration );
  }

  /**
   * Set the transient name.
   *
   * @access protected
   * @since  1.0
   */
  protected function setTransientName() {
    $url = \esc_url( \get_bloginfo( 'url' ) . $_SERVER[ 'REQUEST_URI' ] );
    $this->transient_name = 'cache_' . md5( $url );
  }

  /**
   * Return whether the transient exists.
   *
   * @access protected
   * @since  1.0
   *
   * @return bool Whether the transient exists.
   */
  protected function hasTransient() : bool {
    return false !== ( $this->transient = \get_transient( $this->transient_name ) );
  }

  /**
   * Return the transient.
   *
   * @access protected
   * @since  1.0
   *
   * @return string The transient.
   */
  protected function getTransient() : string {
    return isset( $this->transient ) ? $this->transient : \get_transient( $this->transient_name );
  }
}
