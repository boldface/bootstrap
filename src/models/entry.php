<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Entry model
 *
 * @since 1.0
 */
class entry extends abstractModels {

  /**
   * Entry header model
   *
   * @access public
   * @since  1.0
   *
   * @param string $header The header
   *
   * @return string The new header
   */
  public function header( string $header ) : string {
    $title = \the_title( '', '', false ) ?: ( ! \is_singular() ? \get_the_date() : '' );
    if( \is_singular() ) return $title;
    return sprintf(
      '<a href="%1$s">%2$s</a>',
      \get_the_permalink(),
      $title
    );
  }

  /**
   * Entry content model
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The content
   *
   * @return string The filtered content
   */
  public function content( string $content ) : string {
    return \apply_filters( 'the_content', \get_the_content() );
  }

  /**
   * Return the entry class.
   *
   * @access public
   * @since  1.0
   *
   * @param string $class The old entry class.
   *
   * @return string The entry class.
   */
  public function entryClass( string $class ) : string {
    return trim( $class . ' ' . \get_post()->post_name );
  }

  /**
   * Automatically add paragraph HTML tags to the content.
   *
   * Remove all line breaks. Replace paragraph and h2 elements with full-width rows.
   * Wrap any newlines that don't start with a tag with paragraph tags.
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The content.
   *
   * @return string The new content.
   */
  public function autoRow( string $content ) : string {

    /**
     * Filter whether to do the autoRow procedure.
     *
     * @since 1.0
     *
     * @param bool Whether to do the autoRow procedure. Default true.
     */
    if( ! \apply_filters( 'Boldface\Bootstrap\Models\entry\autoRow', true ) ) return $content;

    $content = str_replace( '<br />', '', $content );
    $content = str_replace( '<br/>', '', $content );
    $content = str_replace( '<br>', '', $content );
    $content = $this->wrapElement( $content );
    return array_reduce( explode( "\n", $content ), function( $carry, $item ) {
      if( 0 === strpos( $item, '<img' ) ) {
        $imgEnd = strpos( $item, '>', 5 );
        $item = substr( $item, 0, $imgEnd + 1 ) . '<p>' . substr( $item, $imgEnd + 1 ) . '</p>';
      }
      return ( 0 !== strpos( $item, '<' ) ) ? $carry . sprintf( '<p>%1$s</p>', $item ) : $carry . $item;
    }, '' );
  }

  /**
   * Wrap elements with full-width rows.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $content The content.
   *
   * @return string The new content.
   */
  protected function wrapElement( string $content ) : string {
    if( '' === $content ) return $content;

    $tags = [
      'h1',
      'h2',
      'h3',
      'h4',
      'h5',
      'h6',
      'ol',
      'ul',
      'p',
      'section',
    ];

    $dom = new \DOMDocument();
    libxml_use_internal_errors( true );
    $dom->loadHTML( "<html>$content</html>", LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD );

    $col = $dom->createElement( 'div' );
    $col->setAttribute( 'class', 'col-md-12' );

    $row = $dom->createElement( 'div' );
    $row->setAttribute( 'class', 'row' );

    foreach( $tags as $tag ) {
      $elements = $dom->getElementsByTagName( $tag );
      foreach( $elements as $element ) {
        $a = $row->cloneNode();
        $b = $col->cloneNode();
        $a->appendChild( $b );
        $element->parentNode->replaceChild( $a, $element );
        $b->appendChild( $element );
      }
    }

    $html = $dom->saveHTML();
    return str_replace( [ '<html>', '</html>' ], '', $html );
  }

  /**
   * Filter the post class
   *
   * @access public
   * @since  1.0
   *
   * @param array $class The unfiltered class array
   *
   * @return array The filtered class array
   */
  public function post_class( array $class ) : array {
    foreach( $class as $k => $v ) {
      $class[ $k ] = str_replace( 'post_format-post-format-', '', $v );
      if(
        0 === strpos( $v, 'tag-' ) ||
        0 === strpos( $v, 'category-' ) ||
        0 === strpos( $v, 'status-' ) ||
        0 === strpos( $v, 'format-' ) ||
        0 === strpos( $v, 'post-' ) ||
        0 === strpos( $v, 'type-' )
      ) {
        unset( $class[ $k ] );
      }
    }
    return $class;
  }

  /**
   * Return the content with the tag list appeded to the end
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The unfiltered content
   *
   * @return string The content with the tag list appended
   */
  public function tags( string $content ) : string {
    return $content . \get_the_tag_list( '<p>Tags: ', ', ', '</p>' );
  }

  /**
   * Return the content with the category list appeded to the end
   *
   * @access public
   * @since  1.0
   *
   * @param string $content The unfiltered content
   *
   * @return string The content with the category list appended
   */
  public function categories( string $content ) : string {
    $categories = \get_the_category_list( ', ' );
    if( '' === $categories ) return $content;
    return $content . '<p>Categories: ' . $categories . '</p>';
  }


  /**
   * Return the link pages appended to the end of the HTML;
   *
   * @access public
   * @since 1.0
   *
   * @param string $html The previous HTML.
   *
   * @return string The link pages appended.
   */
  public function linkPages( string $html ) : string {
    return $html . \wp_link_pages( [ 'echo' => 0, ] );
  }
}
