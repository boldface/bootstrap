<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 *  Models for the gallery
 *
 * @since 1.0
 */
class gallery extends abstractModels {

  /**
   * @var The gallery attributes
   *
   * @access public
   * @since  1.0
   */
  protected $atts;

  /**
   * @var The current instance
   *
   * @access public
   * @since  1.0
   */
  protected $instance;

  /**
   * @var The current attachments
   *
   * @access public
   * @since  1.0
   */
  protected $attachments = [];

  /**
   * Return the gallery
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery
   */
  public function gallery( string $output, $atts, int $instance ) : string {

    $this->atts = \shortcode_atts( [
      'order'      => 'ASC',
      'orderby'    => 'menu_order ID',
      'id'         => \get_post()->ID ?: 0,
      'itemtag'    => 'figure',
      'icontag'    => 'div',
      'captiontag' => 'figcaption',
      'columns'    => 3,
      'size'       => 'thumbnail',
      'include'    => '',
      'exclude'    => '',
      'link'       => '',
    ], $atts, 'gallery' );

    $this->instance = $instance;
    $this->attachments = $this->getAttachments();
    return $this->view->gallery();
  }

  /**
   * Return the post attachments
   *
   * This code is copied, with minor edits, from WordPress core.
   *
   * @access public
   * @since  1.0
   *
   * @return array The post attachments
   */
  protected function getAttachments() : array {
    $attr = $this->atts;

    $id = intval( $attr[ 'id' ] );

    if( ! empty( $attr[ 'include' ] ) ) {
      $_attachments = \get_posts( [
        'include'        => $attr[ 'include' ],
        'post_status'    => 'inherit',
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'order'          => $attr['order'],
        'orderby'        => $attr['orderby'],
      ] );
      $attachments = [];
      foreach( $_attachments as $k => $v )
        $attachments[ $v->ID ] = $_attachments[ $k ];
    }
    elseif( ! empty( $attr[ 'exclude' ] ) ) {
      $attachments = \get_children( [
        'post_parent'    => $id,
        'exclude'        => $attr[ 'exclude' ],
        'post_status'    => 'inherit',
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'order'          => $attr[ 'order' ],
        'orderby'        => $attr[ 'orderby' ],
      ] );
    }
    else {
      $attachments = \get_children( [
        'post_parent'    => $id,
        'post_status'    => 'inherit',
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'order'          => $attr[ 'order' ],
        'orderby'        => $attr[ 'orderby' ],
      ] );
    }
    if ( empty( $attachments ) ) {
      return '';
    }
    return $attachments;
  }

  /**
   * Return the gallery ID
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery ID
   */
  public function galleryID() : string {
    return "gallery-{$this->instance}";
  }

  /**
   * Return the gallery class
   *
   * @access public
   * @since  1.0
   *
   * @return The gallery class
   */
  public function galleryClass() : string {
    return "gallery gallery-columns-{$this->atts['columns']} d-flex flex-wrap";
  }

  /**
   * Return the gallery items
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery items
   */
  public function galleryItems() : string {
    return array_reduce( $this->attachments, function( string $carry, \WP_Post $item ) : string {
      $this->post = $item;
      return $carry . $this->view->card();
    }, '' );
  }

  /**
   * Return the gallery image source
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery image source
   */
  public function galleryImgSrc() : string {
    return \wp_get_attachment_url( $this->post->ID );
  }

  /**
   * Return the gallery image alt attribute
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery image alt attribute
   */
  public function galleryImgAlt() : string {
    return trim( strip_tags( \get_post_meta( $this->post->ID, '_wp_attachment_image_alt', true ) ) );
  }

  /**
   * Return the gallery title
   *
   * @access public
   * @since  1.0
   *
   * @return The gallery title
   */
  public function galleryTitle() : string {
    return \get_the_title( $this->post->ID );
  }

  /**
   * Return the gallery text
   *
   * @access public
   * @since  1.0
   *
   * @return string The gallery text
   */
  public function galleryText() : string {
    return \wp_get_attachment_caption( $this->post->ID );
  }

}
