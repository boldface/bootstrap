<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Loop model
 *
 * @since 1.0
 */
class loop extends abstractModels {

  /**
   * Run the WordPress loop
   *
   * @access public
   * @since 1.0
   */
  public function loop() {
    ob_start();
    \do_action( 'Boldface\Bootstrap\Views\loop\start' );
    if( \have_posts() ) : while( \have_posts() ) : \the_post();
      \do_action( 'Boldface\Bootstrap\Views\loop\items' ); endwhile;
    else: \do_action( 'Boldface\Bootstrap\Views\loop\error' );
    endif;
    \do_action( 'Boldface\Bootstrap\Views\loop\end' );
    return ob_get_clean();
  }

  /**
   * Return the page heading
   *
   * @access public
   * @since  1.0
   *
   * @param string $title The heading to be filtered
   *
   * @return string The heading
   */
  public function pageHeading( string $title ) : string {
    return sprintf(
      '<h1 class="%1$s">%2$s</h1>',
      \apply_filters( 'Boldface\Bootstrap\Model\loop\heading\class', 'page-header' ),
      \apply_filters( 'Boldface\Bootstrap\Model\loop\heading', '' )
    );
  }

  /**
   * Return the blog heading.
   *
   * @access public
   * @since  1.0
   *
   * @param string $title The heading to be filtered. Unused.
   *
   * @return string The filtered blog heading.
   */
  public function blogHeading( string $title ) : string {
    return \apply_filters( 'Boldface\Bootstrap\Model\loop\blog\title',  \esc_html__( 'Blog', 'bootstrap' ) );
  }

  /**
   * Return the search heading.
   *
   * @access public
   * @since  1.0
   *
   * @param string $title The heading to be filtered. Unused.
   *
   * @return string The filtered search heading.
   */
  public function searchHeading( string $title ) : string {
    return \apply_filters( 'Boldface\Bootstrap\Model\loop\search\title',  sprintf( '%1$s: %2$s', \esc_html__( 'Search', 'bootstrap' ), \get_search_query() ) );
  }

  /**
   * Return the author heading.
   *
   * @access public
   * @since  1.0
   *
   * @param string $title The heading to be filtered. Unused.
   *
   * @return string The filtered author heading.
   */
  public function authorHeading( string $title ) : string {
    return \apply_filters( 'Boldface\Bootstrap\Model\loop\author\title',  sprintf( '%1$s: %2$s', \esc_html__( 'Author', 'bootstrap' ), \get_the_author() ) );
  }

  /**
   * Return the title for 404 Not Found.
   *
   * @access public
   * @since 1.0
   *
   * @param string $content The content. Unused.
   *
   * @return string The new title.
   */
  public function errorTitle( string $content ) : string {
    return \apply_filters( 'Boldface\Bootstrap\Model\loop\error\title', esc_html__( '404 Not Found', 'bootstrap' ) );
  }

  /**
   * Return the error message for 404 page.
   *
   * @access public
   * @since 1.0
   *
   * @param string $content The content. Unused.
   *
   * @return string The error message.
   */
  public function errorMessage( string $content ) : string {
    return \apply_filters( 'Boldface\Bootstrap\Model\loop\error\message', esc_html__( 'Oops. We couldn\'t find that page.', 'bootstrap' ) );
  }

  /**
   * Return the category title.
   *
   * @access public
   * @since 1.0
   *
   * @param string $title The title. Unused.
   *
   * @return string The category title.
   */
  public function categoryTitle( string $title ) : string {
    return \get_the_archive_title();
  }

  /**
   * Return the tag title.
   *
   * @access public
   * @since 1.0
   *
   * @param string $title The title. Unused.
   *
   * @return string The tag title.
   */
  public function tagTitle( string $title ) : string {
    return \single_tag_title( 'Tag: ', false );
  }

  /**
   * Print the post pagination.
   *
   * @access public
   * @since 1.0
   */
  public function postsPagination() {
    $pagination = \get_the_posts_pagination( [ 'type' => 'list', ] );
    $pagination = str_replace( 'page-numbers', 'page-link', $pagination );
    $pagination = str_replace( "<ul class='page-link'>", '<ul class="pagination pagination-lg justify-content-center">', $pagination );
    $pagination = str_replace( '<li>', '<li class="page-item">', $pagination );
    $pagination = str_replace( 'current', 'active', $pagination );
    print $pagination;
  }

  /**
   * Return the posts pagination template
   *
   * @access public
   * @since 1.0
   *
   * @param string $template The old template.
   * @param string $class    The class of the template.
   *
   * @return string The posts pagination template.
   */
  public function postsPaginationTemplate( string $template, string $class ) : string {
    if( 'pagination' !== $class ) return $template;
    return '<nav aria-label="Post Pagination">%3$s</nav>';
  }
}
