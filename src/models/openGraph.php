<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * openGraph model
 *
 * @since 1.0
 */
class openGraph extends abstractModels {

  /**
   * @var Required Open Graph meta data
   *
   * @access protected
   * @since  1.0
   */
  protected $required = [ 'title', 'type', 'image', 'url', ];

  /**
   * @var The image width
   *
   * @access protected
   * @since  1.0
   */
  protected $imageWidth = '';

  /**
   * @var The image height
   *
   * @access protected
   * @since  1.0
   */
  protected $imageHeight = '';

  /**
   * Determine which Open Graph metadata to print.
   *
   * @access public
   * @since  1.0
   */
  public function openGraph() {
    array_map( function( $graph ) {
      method_exists( $this->view, $graph ) && $this->view->{$graph}();
    }, array_merge( $this->required, \apply_filters( 'Boldface\Bootstrap\Models\openGraph\args', [
      'description',
      'locale',
      'imageType',
      'imageWidth',
      'imageHeight',
    ] ) ) );
  }

  /**
   * Determine the site icon size and save to class properties.
   *
   * @access public
   * @since  1.0
   *
   * @param string $url     The site icon URL
   * @param int    $size    The site icon size (square)
   * @param int    $blog_id The current blog ID
   *
   * @return string The unaltered site icon URL.
   */
  public function siteIconSize( string $url, int $size, int $blog_id ) : string {
    $this->imageHeight = $this->imageWidth = (string) $size;
    return $url;
  }

  /**
   * Return the image width
   *
   * @access public
   * @since  1.0
   *
   * @return string Image width
   */
  public function imageWidth() : string {
    return $this->imageWidth ?: '';
  }

  /**
   * Return the image height
   *
   * @access public
   * @since  1.0
   *
   * @return string Image height
   */
  public function imageHeight() : string {
    return $this->imageHeight ?: '';
  }
}
