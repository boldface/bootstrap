<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * googleFonts model
 *
 * @since 1.0
 */
class googleFonts extends abstractModels {

  /**
   * Set the Google Fonts.
   *
   * @access public
   * @since  1.0
   */
  public function setFonts() {
    /**
     * Filters the Google fonts.
     *
     * @since 1.0.0
     *
     * @param array The Google Fonts.
     */
    $this->view->setFonts( \apply_filters( 'Boldface\Bootstrap\Models\googleFonts', [] ) );
  }
}
