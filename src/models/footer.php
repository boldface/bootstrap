<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Footer model
 *
 * @since 1.0
 */
class footer extends abstractModels {

  /**
   * Enqueue the popper and bootstrap scripts in the footer
   *
   * @access public
   * @since  1.0
   */
  public function enqueueScripts() {
    \wp_enqueue_style( 'booter', \esc_url( \get_template_directory_uri() . '/assets/css/style.css' ), [ 'bootstrap' ] );
    \wp_enqueue_script( 'popper', $this->popperJavascriptURL(), [ 'jquery' ], null, true );
    \wp_enqueue_script( 'bootstrap', $this->bootstrapJavascriptURL(), [ 'jquery', 'popper' ], null, true );
    \wp_enqueue_script( 'ready', \esc_url( \get_template_directory_uri() . '/assets/js/ready.js' ), [ 'jquery' ] );
    if( \is_singular() && \have_comments() ) \wp_enqueue_script( 'comment-reply' );

    if( \get_stylesheet() !== \get_template() ) {
      if( file_exists( \get_stylesheet_directory() . '/assets/css/style.css' ) ) {
        \wp_enqueue_style( \get_stylesheet(), \get_stylesheet_directory_uri() . '/assets/css/style.css', [ 'booter' ]  );
      }
      if( file_exists( \get_stylesheet_directory() . '/assets/js/script.js' ) ) {
        \wp_enqueue_script( \get_stylesheet(), \get_stylesheet_directory_uri() . '/assets/js/script.js', [ 'bootstrap' ]  );
      }
    }
  }

  /**
   * Add inline script that requires jQuery.
   * Enqueue the Bootstrap CSS. The wp_enqueue_style function does take a
   * parameter to enqueue the style in the footer, so this method must be
   * hooked after wp_head.
   * Add inline style to the bootstrap CSS.
   *
   * @access public
   * @since  1.0
   */
  public function footer() {
    \wp_add_inline_script( 'jquery', 'document.addEventListener("DOMContentLoaded",function(e){var a=document.getElementsByClassName("no-js");a[0].className=a[0].className.replace("no-js","js");});' );
    \wp_enqueue_style( 'bootstrap', $this->bootstrapCSSURL() );
    \wp_add_inline_style( 'booter', \apply_filters( 'Boldface\Bootstrap\Models\footer\css', '' ) );
  }

  /**
   * Return the Popper Javascript URL.
   *
   * @access protected
   * @since  1.0
   *
   * @return string Popper Javascript URL.
   */
  protected function popperJavascriptURL() {
    return \apply_filters( 'Boldface\Bootstrap\localAssets', false ) ?
      \esc_url( \get_template_directory_uri() . '/assets/js/popper.min.js' ) :
      'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js';
  }

  /**
   * Return the Bootstrap Javascript URL.
   *
   * @access protected
   * @since  1.0
   *
   * @return string Bootstrap Javascript URL.
   */
  protected function bootstrapJavascriptURL() {
    return \apply_filters( 'Boldface\Bootstrap\localAssets', false ) ?
      \esc_url( \get_template_directory_uri() . '/assets/js/bootstrap.min.js' ) :
      'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js';
  }

  /**
   * Return the Bootstrap CSS URL.
   *
   * @access protected
   * @since  1.0
   *
   * @return string Bootstrap CSS URL.
   */
  protected function bootstrapCSSURL() {
    return \apply_filters( 'Boldface\Bootstrap\localAssets', false ) ?
      \esc_url( \get_template_directory_uri() . '/assets/css/bootstrap.min.css' ) :
      'https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css';
  }
}
