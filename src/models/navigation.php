<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * Navigation model
 *
 * @since 1.0
 */
class navigation extends abstractModels {

  /**
   * Register the navigation menus
   *
   * @access public
   * @since  1.0
   */
  public function registerNavMenus() {
    \register_nav_menus( \apply_filters( 'Boldface\Bootstrap\navigation\menus', [
      'primary' => esc_html__( 'Primary Menu', 'bootstrap' ),
    ] ) );
  }

  /**
   * Return the primary navigation menu
   *
   * @access public
   * @since  1.0
   *
   * @param string $menu The menu
   *
   * @return The primary navigation menu
   */
  public function menu( string $menu ) : string {
    $nav = "primary";
    $args = \apply_filters( 'Boldface\Bootstrap\Models\navigation\menu\args', [
      'menu' => $nav,
      'theme_location' => $nav,
      'container_id' => 'navbarCollapse',
      'container_class' => 'collapse navbar-collapse',
      'menu_class' => 'navbar-nav ml-auto',
      'menu_id' => $nav . '-menu',
      'echo' => false,
      'fallback_cb' => false,
    ] );
    return is_string( $nav_menu = \wp_nav_menu( $args ) ) ? $nav_menu : '';
  }

  /**
   * Return the brand
   *
   * @access public
   * @since  1.0
   *
   * @param string $brand The brand
   *
   * @return string The brand
   */
  public function brand( string $brand ) : string {

    /**
     * Filter the brand HTML.
     *
     * @since 1.0.0
     *
     * @param string The brand HTML.
     */
    if( $brand = \apply_filters( 'Boldface\Bootstrap\Models\navigation\brand', '' ) )
      return $brand;

    if( file_exists( \get_stylesheet_directory() . '/assets/images/logo.svg' ) ) {
      return sprintf(
        '<object type="image/svg+xml" data="%1$s" class="img img-fluid img-brand"></object>',
        \esc_url( \get_stylesheet_directory_uri() . '/assets/images/logo.svg' )
      );
    }
    if( file_exists( \get_stylesheet_directory() . '/assets/images/logo.png' ) ) {
      return sprintf(
        '<img width="50" height="50" src="%1$s" alt="%2$s" class="img img-fluid img-brand">',
        \esc_url( \get_stylesheet_directory_uri() . '/assets/images/logo.png' ),
        \apply_filters( 'Boldface\Bootstrap\Models\navigation\brand\alt', 'Bootstrap Brand Logo' )
      );
    }
    if( $logo = \get_theme_mod( 'custom_logo' ) ) {
      return sprintf(
        '<img src="%1$s" alt="%2$s" class="img img-fluid img-brand" />',
        \wp_get_attachment_image_src( $logo, 'full' )[0],
        \apply_filters( 'Boldface\Bootstrap\Models\navigation\brand\alt', 'Bootstrap Brand Logo' )
      );
    }
    return \get_bloginfo( 'name' );
  }

  /**
   * Return empty string for menu ID
   *
   * @access public
   * @since  1.0
   *
   * @param string $id The menu item ID
   *
   * @return string Empty string
   */
  public function nav_menu_item_id( string $id ) : string {
    return '';
  }

  /**
   * Return active for the current menu item
   *
   * @access public
   * @since  1.0
   *
   * @param array $classes Array of classes for the menu items
   *
   * @return array Array of classes
   */
  public function nav_menu_items( array $classes ) : array {
    $new = array_filter( $classes, function( $c ) {
      return false === stripos( $c, 'menu-item' ) && false === stripos( $c, 'page' );
    });
    if( in_array( 'current-menu-item', $classes ) ) {
      $new[] = 'active';
    }
    $new[] = 'nav-item';
    return $new;
  }

  /**
   * Add nav-link class to the navigation links
   *
   * @access public
   * @since  1.0
   *
   * @param array $atts Array of attributes
   *
   * @return array Array of attributes for the nav menu links
   */
  public function nav_menu_link_attributes( array $atts ) : array {
    $atts['class'] = 'nav-link';
    return $atts;
  }

  /**
   * Add classes to submenu navigation
   *
   * @access public
   * @since  1.0
   *
   * @param array $classes Array of classes
   * @param array $args    Array of arguments
   * @param int   $depth   Depth of submenu
   *
   * @return array Array of classes for the submenu
   */
  public function nav_menu_submenu_css_class( array $classes, array $args = [], int $depth = 0 ) : array {
    $classes[] = 'dropdown-menu';
    $classes[] = 'bg-dark';
    return $classes;
  }
}
