<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Models;

/**
 * contactForm7 model
 *
 * @since 1.0
 */
class contactForm7 extends abstractModels {

  /**
   * Dequeue the Contact Form 7 style and script
   *
   * @access public
   * @since  1.0
   */
  public function dequeue() {
    \wp_dequeue_script( 'contact-form-7' );
    \wp_dequeue_style( 'contact-form-7' );
  }

  /**
   * Return the class
   *
   * @access public
   * @since  1.0
   *
   * @param  string $attr The class
   *
   * @return string The new class
   */
  public function class_attr( string $attr ) : string {
    return '';
  }

  /**
   * Filter the Contact Form 7 elements to be compaitible with Bootstrap
   *
   * @access public
   * @since  1.0
   *
   * @param  string $elements The unfiltered elements
   *
   * @return string The filtered elements
   */
  public function elements( string $elements ) : string {
    $inner = array_reduce( explode( "<label>", $elements ), function( $carry, $item ) {
      if( false === strpos( $item, '</label>' ) ) return;
      if( false !== strpos( $item, 'recaptcha' ) ) {
        return $carry . $item;
      }
      if( false !== strpos( $item, '<select' ) ) {
        return sprintf(
          '%1$s<div class="form-group"><label for="%2$s">%3$s</label><select id="%2$s" class="%4$s" name="%2$s">%5$s</select></div>',
          $carry,
          $this->find( 'name', $item ),
          $this->label( $item ),
          'form-control custom-select',
          $this->options( $item )
        );
      }
      if( false !== strpos( $item, '<textarea' ) ) {
        return sprintf(
          '%1$s<div class="form-group"><label for="%2$s">%3$s</label><textarea id="%2$s" class="%4$s" name="%2$s" rows="10"></textarea></div>',
          $carry,
          $this->find( 'name', $item ),
          $this->label( $item ),
          'form-control'
        );
      }

      $label = $this->view->label( $this->label( $item ), $this->find( 'name', $item ) );
      $input = $this->view->input( [
        'type' => $this->find( 'type', $item ),
        'id' => $this->find( 'name', $item ),
        'name' => $this->find( 'name', $item ),
        'class' => 'form-control',
        'aria-required' => ( $a = $this->find( 'aria-required', $item ) ) ? $a : 'false'
      ] );

      return $carry . $this->view->form_group( $label, $input );
    } );
    $inner = $this->removeSend( $inner );
    $class = \apply_filters( 'Boldface\Bootstrap\Models\contactForm7\button\class', 'btn btn-primary bg-inverse' );
    return sprintf( '%1$s<button type="submit" class="%2$s">Submit</button>', $inner, $class );
  }

  /**
   * Remove the input submit type from a string.
   *
   * @access protected
   * @since  1.0
   *
   * @param string $input The string to remove the submit from.
   *
   * @return string
   */
  protected function removeSend( string $input ) : string {
    $start = strpos( $input, '<input type="submit"' );
    if( false === $start ) return $input;
    $stop = strpos( $input, '>', $start + 1 );
    if( false === $stop ) return $input;
    $send = substr( $input, $start, $stop - $start + 1 );
    return trim( str_replace( $send, '', $input ) );
  }

  /**
   * Find the needle attribute in the haystack
   *
   * @access public
   * @since  1.0
   *
   * @param string $needle   The needle you want to fine
   * @param string $haystack The haystack in which to find the needle
   *
   * @return string The needle attribure
   */
  protected function find( string $needle, string $haystack ) : string {
    $needle .= '="';
    $start = strpos( $haystack, $needle );
    if( false === $start ) return '';
    $stop  = strpos( $haystack, '"', $start + strlen( $needle ) );
    return substr( $haystack, $start + strlen( $needle ), $stop - $start - strlen( $needle ) );
  }

  /**
   * Return the label from the item
   *
   * @access public
   * @since  1.0
   *
   * @param string $item The item of which to find the label
   *
   * @return string The label
   */
  protected function label( string $item ) : string {
    return \wp_strip_all_tags( substr( $item, 0, strpos( $item, '<span' ) ) );
  }

  /**
   * Return the options from the item
   *
   * @access public
   * @since  1.0
   *
   * @param string $item The item of which to find the label
   *
   * @return string The options
   */
  protected function options( string $item ) : string {
    $start = strpos( $item, '<option' );
    $stop = strrpos( $item, '</option>' );
    return substr( $item, $start, $stop - $start + strlen( '</option>' ) );
  }
}
