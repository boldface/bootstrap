<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for setting up the module.
 *
 * @since 1.0
 */
class module {

  /**
   * @var Template directory
   *
   * @access protected
   * @since  1.0
   */
  protected $template_dir;

  /**
   * @var Source directory
   *
   * @access protected
   * @since  1.0
   */
  protected $source_dir;

  /**
   * @var Boolean whether the module has already been setup
   *
   * @access protected
   * @since  1.0
   */
  protected $hasBeenSetup = false;

  /**
   * @var Module name
   *
   * @access protected
   * @since  1.0
   */
  protected $moduleName;

  /**
   * @var The module object
   *
   * @access protected
   * @since  1.0
   */
  protected $module;

  /**
   * @var The module namespace
   *
   * @access protected
   * @since  1.0
   */
  protected $namespace;

  /**
   * Setup the module.
   *
   * Checks whether the module object has already been setup. If it has, return
   * early. Otherwise, require the necessary files to build the module and
   * flip the hasBeenSetup flag to true.
   *
   * @access protected
   * @since  1.0
   */
  protected function setupModule() {
    if( true === $this->hasBeenSetup ) return;

    $this->setupDirectories();
    $this->hasBeenSetup = true;
  }

  /**
   * Setup the directories.
   *
   * @access protected
   * @since  1.0
   */
  protected function setupDirectories() {
    $this->template_dir    = \trailingslashit( \get_template_directory() );
    if( ! $this->source_dir ) $this->source_dir = \trailingslashit( $this->template_dir . '/src' );
  }

  /**
   * Build the module.
   *
   * @access protected
   * @since  1.0
   */
  protected function buildModule() {
    $controller = "{$this->namespace}\Controllers\\{$this->moduleName}";
    $model      = "{$this->namespace}\Models\\{$this->moduleName}";
    $view       = "{$this->namespace}\Views\\{$this->moduleName}";

    $this->module = new $controller( new $model( new $view() ) );
  }

  /**
   * Use the default namespace if one isn't provided.
   *
   * @access public
   * @since  1.0
   *
   * @return $this.
   */
  public function useDefaultNamespace() {
    if( is_numeric( $this->moduleName ) && isset( $this->namespace ) ) {
      $this->setModuleName( $this->namespace );
      $this->setNamespace( '\Boldface\Bootstrap' );
    }
    return $this;
  }

  /**
   * Set the source directory.
   *
   * @access public
   * @since  1.0
   *
   * @param The source directory.
   *
   * @return $this.
   */
  public function setSourceDir( string $source_dir ) : module {
    $this->source_dir = $source_dir;
    return $this;
  }

  /**
   * Set the module name.
   *
   * @access public
   * @since  1.0
   *
   * @param The source directory.
   *
   * @return $this.
   */
  public function setModuleName( string $moduleName ) : module {
    $this->moduleName = $moduleName;
    return $this;
  }

  /**
   * Get the module name.
   *
   * @access public
   * @since  1.0
   *
   * @param The source directory.
   *
   * @return $this.
   */
  public function getModuleName() : string {
    return $this->moduleName ?: '';
  }

  /**
   * Set the namespace.
   *
   * @access public
   * @since  1.0
   *
   * @param The source directory.
   *
   * @return $this.
   */
  public function setNamespace( string $namespace ) : module {
    $this->namespace = $namespace;
    return $this;
  }

  /**
   * Get the namespace.
   *
   * @access public
   * @since  1.0
   *
   * @param The source directory.
   *
   * @return $this.
   */
  public function getNamespace() : string {
    return $this->namespace ?: '';
  }

  /**
   * Get the module.
   *
   * @access public
   * @since  1.0
   */
  public function getModule() {
    if( isset( $this->module ) ) return $this->module;

    $this->useDefaultNamespace();
    $this->setupModule();
    $this->buildModule();

    return $this->module;
  }
}
