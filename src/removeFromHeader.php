<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

/**
 * Class for cleaning up the header
 *
 * @since 0.1
 */
class removeFromHeader {

  public function init() {
    /* Disable Admin Bar */
    \add_filter( 'show_admin_bar', '__return_false' );

    /* Remove Resource Hints */
    \remove_action( 'wp_head', 'wp_resource_hints', 2 );

    /* Remove WordPress generator */
    \remove_action( 'wp_head', 'wp_generator' );

    /* Remove RSD link */
    \remove_action( 'wp_head', 'rsd_link' );

    /* Remove Feed links */
    \remove_action( 'wp_head', 'feed_links', 2 );
    \remove_action( 'wp_head', 'feed_links_extra', 3);

    /* Remove index link */
    \remove_action( 'wp_head', 'index_rel_link' );

    /* Remove wlwmanifest link */
    \remove_action( 'wp_head', 'wlwmanifest_link' );

    /* Remove parent post link */
    \remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );

    /* Remove prev and next post links */
    \remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head' );

    /* Remove shortlink */
    \remove_action( 'wp_head', 'wp_shortlink_wp_head' );

    /* Remove REST API link */
    \remove_action( 'wp_head', 'rest_output_link_wp_head', 10, 0 );

    /* Remove canonical link */
    \remove_action( 'wp_head', 'rel_canonical' );

    /* Remove oembed discovery links */
    \remove_action ( 'wp-head', 'wp_oembed_add_discovery_links', 10, 1 );

    /* Disable Emoji Support */
    \remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    \remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    \remove_action( 'wp_print_styles', 'print_emoji_styles' );
    \remove_action( 'admin_print_styles', 'print_emoji_styles' );
    \remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    \remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    \remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );


    // Remove Feed Generator Tags
    \remove_action( 'rss2_head', 'the_generator' );
    \remove_action( 'commentsrss2_head', 'the_generator' );
    \remove_action( 'rss_head', 'the_generator' );
    \remove_action( 'rdf_header', 'the_generator' );
    \remove_action( 'atom_head', 'the_generator' );
    \remove_action( 'comments_atom_head', 'the_generator' );
    \remove_action( 'opml_head', 'the_generator' );
    \remove_action( 'app_head', 'the_generator' );

    \remove_action( 'wp_head', 'rest_output_link_wp_head' );
    \remove_action( 'wp_head', 'wp_oembed_add_discovery_links' );
    \remove_action( 'template_redirect', 'rest_output_link_header', 11, 0 );

    \add_filter( 'tiny_mce_plugins', [ $this, 'disableEmojisTinymce' ] );
  }

  public function wp_enqueue_scripts() {
    /* Remove WP Embed */
    \wp_deregister_script( 'wp-embed' );
  }

  public function wp_print_styles() {
    /* Remove Genericons */
    \wp_dequeue_style( 'genericons' );
  }

  public function disableEmojisTinymce( $plugins ) {
    if ( is_array( $plugins ) ) {
  		return array_diff( $plugins, [ 'wpemoji' ] );
  	}
    return $plugins;
  }
}
