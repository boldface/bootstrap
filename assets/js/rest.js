document.addEventListener( 'DOMContentLoaded', function(event) {
  var a = document.getElementsByTagName('a');
  for(var i=0; i<a.length; i++){
    a[i].addEventListener('click',function(e){
      if( -1 === this.href.indexOf( window.location.hostname ) ) { return true; }

      this.href = trimTrailingSlash( this.href );

      var permalink = getPermalinkFromURL( this.href );

      var slug = this.href.slice( this.href.lastIndexOf( '/' ) + 1 );
      doFetchUpdate( slug );

      var stateObj = { slug: slug };
      history.pushState( stateObj, slug, absoluteURLfromSlug( permalink ) );

      jQuery('nav .active').removeClass('active');
      jQuery(this).addClass('active');
      //replaceActiveNav(slug);

      e.preventDefault();
      return false;
    });
  }

  window.onpopstate = function(event) {
    if( 0 == event.state.length ){ return; }
    doFetchUpdate( event.state.slug );
  };
});

function doFetchUpdate( slug ) {
  var url = restURLfromSlug( slug );

  var opts = { method: 'GET', headers: {} };
  fetch( url, opts ).then( function( response ) { return response.json(); })
  .then( function( body ) {
    if( 0 === body.length ) { return; }
    replaceHeadingContent( body[0].post_title );
    replaceEntryContent( body[0].post_content );
    //replaceActiveNav(slug);
  });
}

function trimTrailingSlash( string ) {
  if( string.charAt( string.length - 1 ) == '/' ) {
    string = string.substr( 0, string.length -1 );
  }
  return string;
}

function host() {
  return window.location.host.replace(/^www/,'');
}

function restBase() {
  return '/wp-json/Boldface/Bootstrap/v1/';
}

function untrailingslashit( str ) {
  return str.replace(/\/+$/g,"");
}

function trailingslashit( str ) {
  return untrailingslashit( str ) + '/';
}

function getPermalinkFromURL( url ) {
  return trailingslashit( url.replace( document.location.origin, '' ) );
}

function absoluteURLfromSlug( slug ) {
  return trailingslashit( window.location.protocol + '//' + host() + slug );
}

function restURLfromSlug( slug ) {
  if( '' === slug ) { slug = 'use-front-page'; }
  return restBase() + "get/" + slug + "/";
}

function replaceHeadingContent( heading ) {
  var entryHeading = document.getElementsByClassName('entry-heading');
  if( 0 === entryHeading.length ) { return; }
  entryHeading[0].innerHTML = heading;
}

function replaceEntryContent( content ) {
  var entryContent = document.getElementsByClassName('entry-content');
  if( 0 === entryContent.length ) { return; }
  entryContent[0].innerHTML = content;
}

function replaceActiveNav( slug ) {
  var active = document.getElementsByClassName('active');
  for(var j=0; j<active.length; j++){
    active[j].classList.remove('active');
  }
}
