(function($) {
  $( document ).ready( function() {
    $( window ).scroll( function() {
      var st = $(this).scrollTop();
      var wh = $(document).height() - $(window).height();
      var perc = Math.round( st * 100 / wh );
      $( ".progress-bar" ).css( 'width', perc + '%' );
      $( ".progress-bar" ).attr( 'aria-valuenow', '' + perc );
    });
  });
})(jQuery);
