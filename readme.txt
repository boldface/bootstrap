=== Bootstrap ===
Contributors: Nathan Johnson
Requires at least: WordPress 4.8
Tested up to: WordPress 4.8.2
Stable tag: trunk
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/glp-2.0.html
Tags: one-column, custom-header, custom-menu, custom-logo, featured-images, full-width-template, sticky-post, translation-ready

== Description ==

== Installation ==

1. [Download](https://gitlab.com/boldface/bootstrap/repository/archive.zip) the project ZIP file
2. Extract the ZIP file in your wp-content/themes/ directory
3. Rename the extracted directory to 'bootstrap'
4. Navigate in your browser to your wp-admin/themes.php page and activate the theme

== Changelog ==

= 1.0 =
Initial release
