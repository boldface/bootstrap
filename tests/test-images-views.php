<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestImagesViews extends \WP_UnitTestCase {

  function __construct() {
    $this->imagesViews = new \Boldface\Bootstrap\Views\images();
    parent::__construct();
  }

  function testCaption() {
    $caption = $this->imagesViews->caption( $id = 'testCaption', 'caption-class', '<img>', 'This is the figure caption.' );
    $this->assertSame( '<figure id="testCaption" class="caption-class"><img><figcaption>This is the figure caption.</figcaption></figure>', $caption );
  }
}
