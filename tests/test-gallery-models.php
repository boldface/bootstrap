<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestGalleryModels extends \WP_UnitTestCase {

  function __construct() {
    $this->galleryViews = new \Boldface\Bootstrap\Views\gallery();
    $this->galleryModels = new \Boldface\Bootstrap\Models\gallery( $this->entryViews );
    parent::__construct();
  }

  function testGalleryID() {
    $galleryID = $this->galleryModels->galleryID();
    $this->assertStringStartsWith( 'gallery-', $galleryID );
  }
}
