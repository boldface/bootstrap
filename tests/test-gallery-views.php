<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestGalleryViews extends \WP_UnitTestCase {

  function __construct() {
    $this->galleryViews = new \Boldface\Bootstrap\Views\gallery();
    parent::__construct();
  }

  function testGallery() {
    $gallery = $this->galleryViews->gallery();
    $this->assertSame( '<div id="" class="gallery"></div>', $gallery );
  }

  function testGalleryFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery', function() { return 'gallery'; } );
    $gallery = $this->galleryViews->gallery();
    $this->assertSame( '<div id="" class="gallery">gallery</div>', $gallery );
  }

  function testGalleryIDFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery\id', function() { return 'id'; } );
    $gallery = $this->galleryViews->gallery();
    $this->assertSame( '<div id="id" class="gallery"></div>', $gallery );
  }

  function testGalleryClassFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery\class', '__return_empty_string' );
    $gallery = $this->galleryViews->gallery();
    $this->assertSame( '<div id="" class=""></div>', $gallery );
  }

  function testGalleryCard() {
    $card = $this->galleryViews->card();
    $this->assertSame( '
      <div class="card">
        <img class="card-img-top" src="" alt="">
        <div class="card-body">
          <h4 class="card-title"></h4>
          <p class="card-text"></p>
        </div>
      </div>', $card );
  }

  function testGalleryCardImgSrc() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery\img\src', function() { return 'src'; } );
    $card = $this->galleryViews->card();
    $this->assertSame( '
      <div class="card">
        <img class="card-img-top" src="src" alt="">
        <div class="card-body">
          <h4 class="card-title"></h4>
          <p class="card-text"></p>
        </div>
      </div>', $card );
  }

  function testGalleryCardImgAlt() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery\img\alt', function() { return 'alt'; } );
    $card = $this->galleryViews->card();
    $this->assertSame( '
      <div class="card">
        <img class="card-img-top" src="" alt="alt">
        <div class="card-body">
          <h4 class="card-title"></h4>
          <p class="card-text"></p>
        </div>
      </div>', $card );
  }

  function testGalleryCardTitle() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery\title', function() { return 'title'; } );
    $card = $this->galleryViews->card();
    $this->assertSame( '
      <div class="card">
        <img class="card-img-top" src="" alt="">
        <div class="card-body">
          <h4 class="card-title">title</h4>
          <p class="card-text"></p>
        </div>
      </div>', $card );
  }

  function testGalleryCardText() {
    \add_filter( 'Boldface\Bootstrap\Views\gallery\text', function() { return 'text'; } );
    $card = $this->galleryViews->card();
    $this->assertSame( '
      <div class="card">
        <img class="card-img-top" src="" alt="">
        <div class="card-body">
          <h4 class="card-title"></h4>
          <p class="card-text">text</p>
        </div>
      </div>', $card );
  }
}
