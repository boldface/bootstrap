<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestFooterModels extends \WP_UnitTestCase {

  function __construct() {
    $this->footerViews = new \Boldface\Bootstrap\Views\footer();
    $this->footerModels = new \Boldface\Bootstrap\Models\footer( $this->entryViews );
    parent::__construct();
  }

  function testEnqueueScripts() {
    $this->footerModels->enqueueScripts();
    $this->assertTrue( wp_style_is( 'booter' ) );
    $this->assertTrue( wp_script_is( 'popper' ) );
    $this->assertTrue( wp_script_is( 'bootstrap' ) );
  }
}
