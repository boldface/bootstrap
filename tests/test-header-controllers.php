<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestHeaderControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->headerViews = new \Boldface\Bootstrap\Views\header();
    $this->headerModels = new \Boldface\Bootstrap\Models\header( $this->headerViews );
    $this->headerControllers = new \Boldface\Bootstrap\Controllers\header( $this->headerModels );
    parent::__construct();
  }

  function testHeaderWP() {
    $this->headerControllers->wp();
    $this->assertSame( 10, has_filter( 'body_class', [ $this->headerControllers->getModel(), 'bodyClass' ] ) );
  }

  function testHeaderPriority() {
    $priority = $this->headerControllers->getPriority();
    $this->assertSame( 0, $priority );
  }
}
