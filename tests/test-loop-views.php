<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestLoopViews extends \WP_UnitTestCase {

  function __construct() {
    $this->loopViews = new \Boldface\Bootstrap\Views\loop();
    parent::__construct();
  }

  function testLoop() {
    $this->loopViews->html();
    $this->expectOutputString( '<main class="container"></main>' );
  }

  function testLoopClass() {
    add_filter( 'Boldface\Bootstrap\Views\loop\class', function() { return 'loop-class-test'; } );
    $this->loopViews->html();
    $this->expectOutputString( '<main class="loop-class-test"></main>' );
  }

  function testLoopFilter() {
    add_filter( 'Boldface\Bootstrap\Views\loop', function() { return 'Loop content'; } );
    $this->loopViews->html();
    $this->expectOutputString( '<main class="container">Loop content</main>' );
  }

  function testLoopPageTitle() {
    $this->loopViews->pageTitle();
    $this->expectOutputString( '<header class="page-title"></header>' );
  }

  function testLoopPageTitleClass() {
    add_filter( 'Boldface\Bootstrap\Views\loop\title\class', function() { return 'page-title-test'; } );
    $this->loopViews->pageTitle();
    $this->expectOutputString( '<header class="page-title-test"></header>' );
  }

  function testLoopPageTitleFilter() {
    add_filter( 'Boldface\Bootstrap\Views\loop\title', function() { return 'Page title'; } );
    $this->loopViews->pageTitle();
    $this->expectOutputString( '<header class="page-title">Page title</header>' );
  }

  function testLoopPageBody() {
    $this->loopViews->pageBody();
    $this->expectOutputString( '<div class="error-body"></div>' );
  }

  function testLoopPageBodyClass() {
    add_filter( 'Boldface\Bootstrap\Views\loop\body\class', function() { return 'error-body-test'; } );
    $this->loopViews->pageBody();
    $this->expectOutputString( '<div class="error-body-test"></div>' );
  }

  function testLoopPageBodyFilter() {
    add_filter( 'Boldface\Bootstrap\Views\loop\body', function() { return 'Error body'; } );
    $this->loopViews->pageBody();
    $this->expectOutputString( '<div class="error-body">Error body</div>' );
  }
}
