<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestPopoversModels extends \WP_UnitTestCase {

  function __construct() {
    $this->popoversViews = new \Boldface\Bootstrap\Views\popovers();
    $this->popoversModels = new \Boldface\Bootstrap\Models\popovers( $this->popoversViews );
    parent::__construct();
  }

  function testPopoversAddInlineScript() {
    $this->popoversModels->addInlineScript();
    $this->assertTrue( wp_script_is( 'jquery' ) );
  }
}
