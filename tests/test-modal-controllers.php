<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestModalControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->modalViews = new \Boldface\Bootstrap\Views\modal();
    $this->modalModels = new \Boldface\Bootstrap\Models\modal( $this->modalViews );
    $this->modalControllers = new \Boldface\Bootstrap\Controllers\modal( $this->modalModels );
    parent::__construct();
  }

  function testModalWP() {
    $this->modalControllers->wp();
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\modal\trigger\attributes', [ $this->modalControllers->getModel(), 'triggerAtts' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\modal\trigger\text', [ $this->modalControllers->getModel(), 'triggerText' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\modal\attributes', [ $this->modalControllers->getModel(), 'modalAtts' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\loop\start', [ $this->modalControllers->getView(), 'trigger' ] ) );
  }

  function testModalPriority() {
    $priority = $this->modalControllers->getPriority();
    $this->assertSame( 99, $priority );
  }
}
