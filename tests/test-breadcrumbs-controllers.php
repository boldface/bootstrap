<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestBreadcrumbsControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->breadcrumbsViews = new \Boldface\Bootstrap\Views\breadcrumbs();
    $this->breadcrumbsModels = new \Boldface\Bootstrap\Models\breadcrumbs( $this->breadcrumbsViews );
    $this->breadcrumbsControllers = new \Boldface\Bootstrap\Controllers\breadcrumbs( $this->breadcrumbsModels );
    parent::__construct();
  }

  function testBreadcrumbsWP() {
    $this->breadcrumbsControllers->wp();
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\breadcrumbs', [ $this->breadcrumbsControllers->getModel(), 'breadcrumbs' ] ) );
  }

  function testBreadcrumbsPriority() {
    $priority = $this->breadcrumbsControllers->getPriority();
    $this->assertSame( 20, $priority );
  }
}
