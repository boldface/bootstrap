<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestFeaturedImageModels extends \WP_UnitTestCase {

  function __construct() {
    $this->featuredImageViews = new \Boldface\Bootstrap\Views\featuredImage();
    $this->featuredImageModels = new \Boldface\Bootstrap\Models\featuredImage( $this->featuredImageViews );
    parent::__construct();
  }

  function testFeaturedImagePostClassLandscape() {
    $this->featuredImageModels->setAspectRatio( 'landscape' );
    $class = $this->featuredImageModels->postClass( [] );
    $this->assertSame( [ 'landscape' ], $class );
  }

  function testFeaturedImagePostClassPortrait() {
    $this->featuredImageModels->setAspectRatio( 'portrait' );
    $class = $this->featuredImageModels->postClass( [] );
    $this->assertSame( [ 'portrait' ], $class );
  }

  function testFeaturedImageWrapperClassLandscape() {
    $this->featuredImageModels->setAspectRatio( 'landscape' );
    $class = $this->featuredImageModels->wrapperClass('');
    $this->assertSame( 'row', $class );
  }

  function testFeaturedImageWrapperClassPortrait() {
    $this->featuredImageModels->setAspectRatio( 'portrait' );
    $class = $this->featuredImageModels->wrapperClass('');
    $this->assertSame( 'float-left', $class );
  }

  function testFeaturedImageClass() {
    $class = $this->featuredImageModels->featuredImageClass('');
    $this->assertSame( '', $class );
  }

  function testFeaturedImageClassLandscape() {
    $this->featuredImageModels->setAspectRatio( 'landscape' );
    $class = $this->featuredImageModels->featuredImageClass('');
    $this->assertSame( 'col-md-12', $class );
  }

  function testFeaturedImageClassPortrait() {
    $this->featuredImageModels->setAspectRatio( 'portrait' );
    $class = $this->featuredImageModels->featuredImageClass('');
    $this->assertSame( '', $class );
  }

  function testFeaturedImageSetAspectRatioLandscape() {
    $this->featuredImageModels->setAspectRatio( 'landscape' );
    $this->assertSame( 'landscape', $this->featuredImageModels->getAspectRatio() );
  }

  function testFeaturedImageSetAspectRatioPortrait() {
    $this->featuredImageModels->setAspectRatio( 'portrait' );
    $this->assertSame( 'portrait', $this->featuredImageModels->getAspectRatio() );
  }

  function testFeaturedImageSetWrongAspectRatio() {
    $this->featuredImageModels->setAspectRatio( 'wrong' );
    $this->assertSame( 'landscape', $this->featuredImageModels->getAspectRatio() );
  }
}
