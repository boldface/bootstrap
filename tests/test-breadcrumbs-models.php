<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestBreadcrumbsModels extends \WP_UnitTestCase {

  function __construct() {
    $this->breadcrumbsViews = new \Boldface\Bootstrap\Views\breadcrumbs();
    $this->breadcrumbsModels = new \Boldface\Bootstrap\Models\breadcrumbs( $this->breadcrumbsViews );
    parent::__construct();
  }

  function testBreadcrumbs() {
    $breadcrumbs = $this->breadcrumbsModels->Breadcrumbs('');
    $this->assertSame( '', $breadcrumbs );
  }
}
