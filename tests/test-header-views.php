<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestHeaderViews extends \WP_UnitTestCase {

  function __construct() {
    $this->headerViews = new \Boldface\Bootstrap\Views\header();
    parent::__construct();
  }

  function testHeaderOutput() {
    ob_start();
    $caption = $this->headerViews->html();
    $header = trim( ob_get_clean() );
    $this->assertStringStartsWith( '<!DOCTYPE html>', $header );
    $this->assertStringEndsWith( '</head>'.PHP_EOL.'<body class="">', $header );
  }
}
