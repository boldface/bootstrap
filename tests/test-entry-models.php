<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestEntryModels extends \WP_UnitTestCase {

  function __construct() {
    $this->entryViews = new \Boldface\Bootstrap\Views\entry();
    $this->entryModels = new \Boldface\Bootstrap\Models\entry( $this->entryViews );
    parent::__construct();
  }

  function testAutoRow() {
    $content = $this->entryModels->autoRow( 'test' );
    $this->assertSame( '<p>test</p>', $content );
  }

  function testAutoRowP() {
    $content = $this->entryModels->autoRow( '<p>test</p>' );
    $this->assertSame( '<div class="row"><div class="col-md-12"><p>test</p></div></div>', $content );
  }

  function testAutoRowBr() {
    $content = $this->entryModels->autoRow( 'test<br /><br/><br>' );
    $this->assertSame( '<p>test</p>', $content );
  }

  function testPostClass() {
    $class = [ 'class' ];
    $this->assertSame( [ 'class' ], $this->entryModels->post_class( $class ) );
  }

  function testPostClassCategory() {
    $class = [ 'category-test' ];
    $this->assertSame( [], $this->entryModels->post_class( $class ) );
  }
}
