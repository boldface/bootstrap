<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestSocialModels extends \WP_UnitTestCase {

  function __construct() {
    $this->socialViews = new \Boldface\Bootstrap\Views\social();
    $this->socialModels = new \Boldface\Bootstrap\Models\social( $this->socialViews );
    parent::__construct();
  }

  function testSocialSetup() {
    $this->socialModels->setUpSocial();
    $this->assertSame( [
      'facebook'  => 'Facebook',
      'twitter'   => 'Twitter',
      'linkedin'  => 'LinkedIn',
      'instagram' => 'Instagram',
      'website'   => 'Website',
    ], $this->socialModels->getSocial() );
  }

  function testSocialSetupFilter() {
    add_filter( 'Boldface\Bootstrap\Models\social', function( $social ) {
      unset( $social[ 'facebook' ] );
      return $social;
    });

    $this->socialModels->setUpSocial();
    $this->assertSame( [
      'twitter'   => 'Twitter',
      'linkedin'  => 'LinkedIn',
      'instagram' => 'Instagram',
      'website'   => 'Website',
    ], $this->socialModels->getSocial() );
  }
}
