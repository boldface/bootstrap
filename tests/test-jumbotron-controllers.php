<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestJumbotronControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->jumbotronViews = new \Boldface\Bootstrap\Views\jumbotron();
    $this->jumbotronModels = new \Boldface\Bootstrap\Models\jumbotron( $this->jumbotronViews );
    $this->jumbotronControllers = new \Boldface\Bootstrap\Controllers\jumbotron( $this->jumbotronModels );
    parent::__construct();
  }

  function testJumbotronWP() {
    $this->jumbotronControllers->wp();
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\jumbotron', [ $this->jumbotronControllers->getView(), 'jumbotronHeader' ] ) );
 }

  function testJumbotronPriority() {
    $priority = $this->jumbotronControllers->getPriority();
    $this->assertSame( 20, $priority );
  }
}
