<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestLoopModels extends \WP_UnitTestCase {

  function __construct() {
    $this->loopViews = new \Boldface\Bootstrap\Views\loop();
    $this->loopModels = new \Boldface\Bootstrap\Models\loop( $this->loopViews );
    parent::__construct();
  }

  function testLoop() {
    $loop = $this->loopModels->loop();
    $this->assertSame( '', $loop );
  }

  function testLoopStartAction() {
    add_action( 'Boldface\Bootstrap\Views\loop\start', function() { echo 'Loop start'; } );
    $loop = $this->loopModels->loop();
    $this->assertSame( 'Loop start', $loop );
  }

  function testLoopEndAction() {
    add_action( 'Boldface\Bootstrap\Views\loop\end', function() { echo 'Loop end'; } );
    $loop = $this->loopModels->loop();
    $this->assertSame( 'Loop end', $loop );
  }

  function testLoopItemsAction() {
    static $i = 0;
    add_action( 'Boldface\Bootstrap\Views\loop\items', function() { echo ++$i; } );
    $loop = $this->loopModels->loop();
    $this->assertSame( '', $loop );
  }

  function testLoopErrorAction() {
    add_action( 'Boldface\Bootstrap\Views\loop\error', function() { echo 'Loop error'; } );
    $loop = $this->loopModels->loop();
    $this->assertSame( 'Loop error', $loop );
  }

  function testLoopPageHeading() {
    $pageHeading = $this->loopModels->pageHeading('');
    $this->assertSame( '<h1 class="page-header"></h1>', $pageHeading );
  }

  function testLoopPageHeadingClass() {
    add_filter( 'Boldface\Bootstrap\Model\loop\heading\class', function() { return 'page-heading-test'; } );
    $pageHeading = $this->loopModels->pageHeading('');
    $this->assertSame( '<h1 class="page-heading-test"></h1>', $pageHeading );
  }

  function testLoopPageHeadingFilter() {
    add_filter( 'Boldface\Bootstrap\Model\loop\heading', function() { return 'Page Heading'; } );
    $pageHeading = $this->loopModels->pageHeading('');
    $this->assertSame( '<h1 class="page-header">Page Heading</h1>', $pageHeading );
  }

  function testLoopAuthorHeading() {
    $authorHeading = $this->loopModels->authorHeading('');
    $this->assertSame( 'Author: ', $authorHeading );
  }

  function testLoopAuthorHeadingFilter() {
    add_filter( 'Boldface\Bootstrap\Model\loop\author\title', function() { return 'Author: Test'; } );
    $authorHeading = $this->loopModels->authorHeading('');
    $this->assertSame( 'Author: Test', $authorHeading );
  }
}
