<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestContactForm7Views extends \WP_UnitTestCase {

  function __construct() {
    $this->contactForm7Views = new \Boldface\Bootstrap\Views\contactForm7();
    parent::__construct();
  }

  function testContactForm7() {
    $elements = $this->contactForm7Views->elements('');
    $this->assertSame( $elements, '<div class="col-md-6"></div>');
  }

  function testContactForm7Filter() {
    add_filter( 'Boldface\Bootstrap\Views\contactForm7\elements', function() { return 'Contact Form 7 content'; } );
    $elements = $this->contactForm7Views->elements('');
    $this->assertSame( $elements, '<div class="col-md-6">Contact Form 7 content</div>');
  }

  function testContactForm7ClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\contactForm7\elements\class', '__return_empty_string' );
    $elements = $this->contactForm7Views->elements('');
    $this->assertSame( $elements, '<div class=""></div>');
  }

  function testContactForm7FormGroup() {
    $formGroup = $this->contactForm7Views->form_group( '<label>Example</label>', '<input>' );
    $this->assertSame( $formGroup, '<div class="form-group"><label>Example</label><input></div>' );
  }

  function testContactForm7Label() {
    $label = $this->contactForm7Views->label( 'Label', 'for' );
    $this->assertSame( $label, '<label for="for">Label</label>' );
  }

  function testContactForm7Input() {
    $input = $this->contactForm7Views->input( [] );
    $this->assertSame( $input, '<input>' );
  }

  function testContactForm7InputArgs() {
    $input = $this->contactForm7Views->input( [ 'class' => 'form-group', 'type' => 'text', ] );
    $this->assertSame( $input, '<input class="form-group" type="text">' );
  }
}
