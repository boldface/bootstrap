<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestOpenGraphControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->openGraphViews = new \Boldface\Bootstrap\Views\openGraph();
    $this->openGraphModels = new \Boldface\Bootstrap\Models\openGraph( $this->openGraphViews );
    $this->openGraphControllers = new \Boldface\Bootstrap\Controllers\openGraph( $this->openGraphModels );
    parent::__construct();
  }

  function testOpenGraph() {
    $this->openGraphControllers->wp();
    $this->assertSame( 10, has_filter( 'wp_head', [ $this->openGraphControllers->getModel(), 'openGraph' ] ) );
    $this->assertSame( 10, has_filter( 'get_site_icon_url', [ $this->openGraphControllers->getModel(), 'siteIconSize' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\openGraph\image:width', [ $this->openGraphControllers->getModel(), 'imageWidth' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\openGraph\image:height', [ $this->openGraphControllers->getModel(), 'imageHeight' ] ) );
  }
}
