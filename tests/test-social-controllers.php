<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestSocialControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->socialViews = new \Boldface\Bootstrap\Views\social();
    $this->socialModels = new \Boldface\Bootstrap\Models\social( $this->socialViews );
    $this->socialControllers = new \Boldface\Bootstrap\Controllers\social( $this->socialModels );
    parent::__construct();
  }

  function testSocialAdminInit() {
    $this->socialControllers->admin_init();
    $this->assertSame( 10, has_filter( 'show_user_profile', [ $this->socialControllers->getView(), 'show_user_profile' ] ) );
    $this->assertSame( 10, has_filter( 'edit_user_profile', [ $this->socialControllers->getView(), 'show_user_profile' ] ) );
    $this->assertSame( 10, has_filter( 'personal_options_update', [ $this->socialControllers->getModel(), 'profile_update' ] ) );
    $this->assertSame( 10, has_filter( 'edit_user_profile_update', [ $this->socialControllers->getModel(), 'profile_update' ] ) );
  }

  function testSocialWP() {
    $this->socialControllers->wp();
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\social', [ $this->socialControllers->getView(), 'social' ] ) );
  }

  function testSocialPriority() {
    $priority = $this->socialControllers->getPriority();
    $this->assertSame( -1, $priority );
  }
}
