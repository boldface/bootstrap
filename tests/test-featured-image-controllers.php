<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestFeaturedImageControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->featuredImageViews = new \Boldface\Bootstrap\Views\featuredImage();
    $this->featuredImageModels = new \Boldface\Bootstrap\Models\featuredImage( $this->featuredImageViews );
    $this->featuredImageControllers = new \Boldface\Bootstrap\Controllers\featuredImage( $this->featuredImageModels );
    parent::__construct();
  }

  function testFeaturedImageAfterSetupTheme() {
    $this->featuredImageControllers->after_setup_theme();
    $this->assertTrue( \current_theme_supports( 'post-thumbnails' ) );
  }

  function testFeaturedImagePriority() {
    $priority = $this->featuredImageControllers->getPriority();
    $this->assertSame( -1, $priority );
  }
}
