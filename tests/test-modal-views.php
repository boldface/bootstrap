<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestModalViews extends \WP_UnitTestCase {

  function __construct() {
    $this->modalViews = new \Boldface\Bootstrap\Views\modal();
    parent::__construct();
  }

  function testModalButton() {
    $this->modalViews->trigger();
    $this->expectOutputString( '<button></button>' );
  }

  function testModalButtonAtts() {
    add_filter( 'Boldface\Bootstrap\Views\modal\trigger\attributes', function() { return [ 'class' => 'btn' ]; });
    $this->modalViews->trigger();
    $this->expectOutputString( '<button class="btn"></button>' );
  }

  function testModalButtonText() {
    add_filter( 'Boldface\Bootstrap\Views\modal\trigger\text', function() { return 'Modal Text'; });
    $this->modalViews->trigger();
    $this->expectOutputString( '<button>Modal Text</button>' );
  }
}
