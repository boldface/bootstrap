<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestImagesControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->imagesViews = new \Boldface\Bootstrap\Views\images();
    $this->imagesModels = new \Boldface\Bootstrap\Models\images( $this->imagesViews );
    $this->imagesControllers = new \Boldface\Bootstrap\Controllers\images( $this->imagesModels );
    parent::__construct();
  }

  function testImagesAdminInit() {
    $this->imagesControllers->admin_init();
    $this->assertSame( 10, has_filter( 'get_image_tag_class', [ $this->imagesControllers->getModel(), 'get_image_tag_class' ] ) );
  }

  function testImagesInit() {
    $this->imagesControllers->init();
    $this->assertSame( 10, has_filter( 'img_caption_shortcode', [ $this->imagesControllers->getModel(), 'img_caption_shortcode' ] ) );
  }

  function testImagesAdminWP() {
    $this->imagesControllers->wp();
    $this->assertSame( 10, has_filter( 'wp_get_attachment_image_attributes', [ $this->imagesControllers->getModel(), 'wp_get_attachment_image_attributes' ] ) );
  }
}
