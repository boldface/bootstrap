<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestNavigationControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->navigationViews = new \Boldface\Bootstrap\Views\navigation();
    $this->navigationModels = new \Boldface\Bootstrap\Models\navigation( $this->navigationViews );
    $this->navigationControllers = new \Boldface\Bootstrap\Controllers\navigation( $this->navigationModels );
    parent::__construct();
  }

  function testNavigationWP() {
    $this->navigationControllers->wp();
    $this->assertSame( 10, has_filter( 'nav_menu_css_class', [ $this->navigationControllers->getModel(), 'nav_menu_items' ] ) );
  }

  function testNavigationPriority() {
    $priority = $this->navigationControllers->getPriority();
    $this->assertSame( 5, $priority );
  }
}
