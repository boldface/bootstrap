<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestCustomHeaderControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->customHeaderViews = new \Boldface\Bootstrap\Views\customHeader();
    $this->customHeaderModels = new \Boldface\Bootstrap\Models\customHeader( $this->customHeaderViews );
    $this->customHeaderControllers = new \Boldface\Bootstrap\Controllers\customHeader( $this->customHeaderModels );
    parent::__construct();
  }

  function testCustomHeaderInit() {
    $this->customHeaderControllers->init();
    $this->assertTrue( \current_theme_supports( 'custom-header' ) );
  }

  function testCustomHeaderWP() {
    $this->customHeaderControllers->wp();
    $customHeader = \has_custom_header() ? 10 : false;
    $this->assertSame( $customHeader, has_filter( 'Boldface\Bootstrap\Views\loop', [ $this->customHeaderControllers->getView(), 'customHeader' ] ) );
  }

  function testCustomHeaderPriority() {
    $priority = $this->customHeaderControllers->getPriority();
    $this->assertSame( -1, $priority );
  }
}
