<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestOpenGraphModels extends \WP_UnitTestCase {

  function __construct() {
    $this->openGraphViews = new \Boldface\Bootstrap\Views\openGraph();
    $this->openGraphModels = new \Boldface\Bootstrap\Models\openGraph( $this->openGraphViews );
    parent::__construct();
  }

  function testOpenGraphIconSize() {
    $url = 'https://example.org/';
    $size = 150;
    $blog_id = 1;
    $newUrl = $this->openGraphModels->siteIconSize( $url, $size, $blog_id );
    $this->assertSame( $url, $newUrl );
    $this->assertSame( '150', $this->openGraphModels->imageWidth() );
    $this->assertSame( '150', $this->openGraphModels->imageHeight() );
  }
}
