<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestPopoversControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->popoverViews = new \Boldface\Bootstrap\Views\popovers();
    $this->popoverModels = new \Boldface\Bootstrap\Models\popovers( $this->popoverViews );
    $this->popoverControllers = new \Boldface\Bootstrap\Controllers\popovers( $this->popoverModels );
    parent::__construct();
  }

  function testPopovers() {
    $this->popoverControllers->wp();
    $this->assertSame( 10, has_filter( 'wp_enqueue_scripts', [ $this->popoverControllers->getModel(), 'addInlineScript' ] ) );
  }
}
