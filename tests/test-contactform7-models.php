<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestContactForm7Models extends \WP_UnitTestCase {

  function __construct() {
    $this->contactForm7Views = new \Boldface\Bootstrap\Views\contactForm7();
    $this->contactForm7Models = new \Boldface\Bootstrap\Models\contactForm7( $this->contactForm7Views );
    parent::__construct();
  }

  function testContactForm7DequeueStyle() {
    $this->contactForm7Models->dequeue();
    $dequeueStyle = \wp_style_is( 'contact-form-7' );
    $this->assertSame( $dequeueStyle, false );
  }

  function testContactForm7DequeueScript() {
    $this->contactForm7Models->dequeue();
    $dequeueScript = \wp_script_is( 'contact-form-7' );
    $this->assertSame( $dequeueScript, false );
  }

  function testContactForm7ClassAttr() {
    $classAttr = $this->contactForm7Models->class_attr( 'Any string.' );
    $this->assertSame( $classAttr, '' );
  }
}
