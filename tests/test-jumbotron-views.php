<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestJumbotronViews extends \WP_UnitTestCase {

  function __construct() {
    $this->jumbotronViews = new \Boldface\Bootstrap\Views\jumbotron();
    parent::__construct();
  }

  function testJumbotron() {
    $this->jumbotronViews->html();
    $this->expectOutputString( '<div class="jumbotron"></div>' );
  }

  function testJumbotronClass() {
    add_filter( 'Boldface\Bootstrap\Views\jumbotron\class', '__return_empty_string' );
    $this->jumbotronViews->html();
    $this->expectOutputString( '<div class=""></div>' );
  }

  function testJumbotronFilter() {
    add_filter( 'Boldface\Bootstrap\Views\jumbotron', function() { return 'Jumbotron!'; } );
    $this->jumbotronViews->html();
    $this->expectOutputString( '<div class="jumbotron">Jumbotron!</div>' );
  }

  function testJumbtronHeader() {
    $jumbotronHeader = $this->jumbotronViews->jumbotronHeader('');
    $this->assertSame( '<h1 class="display-3"></h1>', $jumbotronHeader );
  }

  function testJumbtronHeaderClass() {
    add_filter( 'Boldface\Bootstrap\Views\jumbotron\header\class', function() { return 'test'; } );
    $jumbotronHeader = $this->jumbotronViews->jumbotronHeader('');
    $this->assertSame( '<h1 class="test"></h1>', $jumbotronHeader );
  }

  function testJumbtronHeaderFilter() {
    add_filter( 'Boldface\Bootstrap\Views\jumbotron\header', function() { return 'test'; } );
    $jumbotronHeader = $this->jumbotronViews->jumbotronHeader('');
    $this->assertSame( '<h1 class="display-3">test</h1>', $jumbotronHeader );
  }
}
