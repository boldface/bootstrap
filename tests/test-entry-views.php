<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestEntryViews extends \WP_UnitTestCase {

  function __construct() {
    $this->entryViews = new \Boldface\Bootstrap\Views\entry();
    parent::__construct();
  }

  function testEntry() {
    $this->entryViews->entry();
    $this->expectOutputString( '<article class=""></article>' );
  }

  function testEntryFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry', function() { return 'Entry content'; } );
    $this->entryViews->entry();
    $this->expectOutputString( '<article class="">Entry content</article>' );
  }

  function testEntryClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\class', function() { return 'entry-class'; } );
    $this->entryViews->entry();
    $this->expectOutputString( '<article class="entry-class"></article>' );
  }

  function testEntryHeader() {
    $entryHeader = $this->entryViews->entryHeader('');
    $this->assertSame( $entryHeader, '<header class="entry-header"></header>' );
  }

  function testEntryHeaderFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\header', function() { return 'Entry header content'; } );
    $entryHeader = $this->entryViews->entryHeader('');
    $this->assertSame( $entryHeader, '<header class="entry-header">Entry header content</header>' );
  }

  function testEntryHeaderClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\header\class', function() { return 'entry-header-test'; } );
    $entryHeader = $this->entryViews->entryHeader('');
    $this->assertSame( $entryHeader, '<header class="entry-header-test"></header>' );
  }

  function testEntryHeading() {
    $entryHeading = $this->entryViews->entryHeading('');
    $this->assertSame( $entryHeading, '<h2 class="entry-heading"></h2>');
  }

  function testEntryHeadingFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\heading', function() { return 'Entry heading'; } );
    $entryHeading = $this->entryViews->entryHeading('');
    $this->assertSame( $entryHeading, '<h2 class="entry-heading">Entry heading</h2>');
  }

  function testEntryHeadingClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\heading\class', function() { return 'entry-heading-test'; } );
    $entryHeading = $this->entryViews->entryHeading('');
    $this->assertSame( $entryHeading, '<h2 class="entry-heading-test"></h2>');
  }

  function testEntryHeadingElement() {
    add_filter( 'Boldface\Bootstrap\Views\entry\heading\element', function() { return 'h1'; } );
    $entryHeading = $this->entryViews->entryHeading('');
    $this->assertSame( $entryHeading, '<h1 class="entry-heading"></h1>');
  }

  function testEntryContent() {
    $entryContent = $this->entryViews->entryContent('');
    $this->assertSame( $entryContent, '<div class="entry-content"></div>');
  }

  function testEntryContentFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\content', function() { return 'Entry content'; } );
    $entryContent = $this->entryViews->entryContent('');
    $this->assertSame( $entryContent, '<div class="entry-content">Entry content</div>');
  }

  function testEntryContentClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\content\class', function() { return 'entry-content-test'; } );
    $entryContent = $this->entryViews->entryContent('');
    $this->assertSame( $entryContent, '<div class="entry-content-test"></div>');
  }

  function testEntryFooter() {
    $entryFooter = $this->entryViews->entryFooter('');
    $this->assertSame( $entryFooter, '<footer class="entry-footer"></footer>');
  }

  function testEntryFooterFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\footer', function() { return 'Entry footer'; } );
    $entryFooter = $this->entryViews->entryFooter('');
    $this->assertSame( $entryFooter, '<footer class="entry-footer">Entry footer</footer>');
  }

  function testEntryFooterClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\entry\footer\class', function() { return 'entry-content-test'; } );
    $entryFooter = $this->entryViews->entryFooter('');
    $this->assertSame( $entryFooter, '<footer class="entry-content-test"></footer>');
  }
}
