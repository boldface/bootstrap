<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestBreadcrumbsViews extends \WP_UnitTestCase {

  function __construct() {
    $this->breadcrumbsViews = new \Boldface\Bootstrap\Views\breadcrumbs();
    parent::__construct();
  }

  function testBreadcrumbs() {
    $this->breadcrumbsViews->html();
    $this->expectOutputString( '<ol class="breadcrumb" role="navigation"></ol>' );
  }

  function testBreadcrumbsFilter() {
    add_filter( 'Boldface\Bootstrap\Views\breadcrumbs', function() { return 'Breadcrumbs content'; } );
    $this->breadcrumbsViews->html();
    $this->expectOutputString( '<ol class="breadcrumb" role="navigation">Breadcrumbs content</ol>' );
  }

  function testBreadcrumbsClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\breadcrumbs\class', function() { return 'breadcrumbs-class-test'; } );
    $this->breadcrumbsViews->html();
    $this->expectOutputString( '<ol class="breadcrumbs-class-test" role="navigation"></ol>' );
  }
}
