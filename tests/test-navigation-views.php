<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestNavigationViews extends \WP_UnitTestCase {

  function __construct() {
    $this->navigationViews = new \Boldface\Bootstrap\Views\navigation();
    parent::__construct();
  }

  function testNavigationHtml() {
    ob_start();
    $this->navigationViews->html();
    $navigation = ob_get_clean();
    $this->assertSame(
'<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="http://example.org/">Test Blog</a>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

</nav>' , $navigation );
  }

  function testNavigationHtmlFilter() {
    add_filter( 'Boldface\Bootstrap\Views\navigation\class', '__return_empty_string' );
    ob_start();
    $this->navigationViews->html();
    $navigation = ob_get_clean();
    $this->assertSame(
'<nav class="">
<a class="navbar-brand" href="http://example.org/">Test Blog</a>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

</nav>' , $navigation );
  }

  function testNavigationHtmlBrandFilter() {
    add_filter( 'Boldface\Bootstrap\Views\navigation\brand', '__return_empty_string' );
    ob_start();
    $this->navigationViews->html();
    $navigation = ob_get_clean();
    $this->assertSame(
'<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="http://example.org/"></a>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>

</nav>' , $navigation );
  }

  function testNavigationMenu() {
    add_filter( 'Boldface\Bootstrap\Views\navigation\menu', function() { return 'Test Nav'; } );
    ob_start();
    $this->navigationViews->html();
    $navigation = ob_get_clean();
    $this->assertSame(
'<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">
<a class="navbar-brand" href="http://example.org/">Test Blog</a>
<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
Test Nav
</nav>' , $navigation );
  }
}
