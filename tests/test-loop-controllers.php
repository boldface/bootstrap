<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestLoopControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->loopViews = new \Boldface\Bootstrap\Views\loop();
    $this->loopModels = new \Boldface\Bootstrap\Models\loop( $this->loopViews );
    $this->loopControllers = new \Boldface\Bootstrap\Controllers\loop( $this->loopModels );
    parent::__construct();
  }

  function testLoopWP() {
    $this->loopControllers->wp();
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\loop', [ $this->loopControllers->getModel(), 'loop' ] ) );
  }

  function testLoopPriority() {
    $priority = $this->loopControllers->getPriority();
    $this->assertSame( 50, $priority );
  }
}
