<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestFooterControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->footerViews = new \Boldface\Bootstrap\Views\footer();
    $this->footerModels = new \Boldface\Bootstrap\Models\footer( $this->footerViews );
    $this->footerControllers = new \Boldface\Bootstrap\Controllers\footer( $this->footerModels );
    parent::__construct();
  }

  function testFooterWP() {
    $this->footerControllers->wp();
    $this->assertSame( 10, has_filter( 'wp_enqueue_scripts', [ $this->footerControllers->getModel(), 'enqueueScripts' ] ) );
    $this->assertSame( 10, has_filter( 'wp_footer', [ $this->footerControllers->getModel(), 'footer' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\footer', [ $this->footerControllers->getView(), 'footer' ] ) );
    $this->assertSame( 99, has_filter( 'Boldface\Bootstrap\Models\footer\css', [ $this->footerControllers->getView(), 'formatCSS' ] ) );
  }

  function testFooterPriority() {
    $priority = $this->footerControllers->getPriority();
    $this->assertSame( 99, $priority );
  }
}
