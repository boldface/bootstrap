<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

$abspath = dirname( dirname( dirname( $_SERVER[ 'PWD' ] ) ) );
require_once $abspath . '/phpunit/includes/functions.php';

function _manually_load_environment() {
  \switch_theme( 'bootstrap' );
}
\tests_add_filter( 'muplugins_loaded', __NAMESPACE__ . '\_manually_load_environment' );

function _manually_load_modules( $modules ) {
  $modules[] = 'breadcrumbs';
  $modules[] = 'contactForm7';
  $modules[] = 'customHeader';
  $modules[] = 'featuredImage';
  $modules[] = 'gallery';
  $modules[] = 'googleFonts';
  $modules[] = 'images';
  $modules[] = 'jumbotron';
  $modules[] = 'modal';
  $modules[] = 'openGraph';
  $modules[] = 'popovers';
  $modules[] = 'social';
  return $modules;
}
\tests_add_filter( 'Boldface\Bootstrap\Controllers\modules', __NAMESPACE__ . '\_manually_load_modules' );

require $abspath . '/phpunit/includes/bootstrap.php';
