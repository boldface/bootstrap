<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestCustomHeaderViews extends \WP_UnitTestCase {

  function __construct() {
    $this->customHeaderViews = new \Boldface\Bootstrap\Views\customHeader();
    parent::__construct();
  }

  function testCustomHeader() {
    add_filter( 'Boldface\Bootstrap\Views\customHeader', function() { return (object) [ 'url' => 'https://example.com/image.jpg', ]; } );
    $customHeader = $this->customHeaderViews->customHeader('');
    $this->assertSame( '<div class="d-flex justify-content-center"><img src="https://example.com/image.jpg" height="" width="" alt="" /></div>', $customHeader );
  }

  function testCustomHeaderClass() {
    add_filter( 'Boldface\Bootstrap\Views\customHeader', function() { return (object) [ 'url' => 'https://example.com/image.jpg', ]; } );
    add_filter( 'Boldface\Bootstrap\Views\customHeader\class', '__return_empty_string' );
    $customHeader = $this->customHeaderViews->customHeader('');
    $this->assertSame( '<div class=""><img src="https://example.com/image.jpg" height="" width="" alt="" /></div>', $customHeader );
  }

  function testCustomHeaderAlt() {
    add_filter( 'Boldface\Bootstrap\Views\customHeader', function() { return (object) [ 'url' => 'https://example.com/image.jpg', ]; } );
    add_filter( 'Boldface\Bootstrap\Views\customHeader\alt', function() { return 'customHeader alt'; } );
    $customHeader = $this->customHeaderViews->customHeader('');
    $this->assertSame( '<div class="d-flex justify-content-center"><img src="https://example.com/image.jpg" height="" width="" alt="customHeader alt" /></div>', $customHeader );
  }
}
