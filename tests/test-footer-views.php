<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestfooterViews extends \WP_UnitTestCase {

  function __construct() {
    $this->footerViews = new \Boldface\Bootstrap\Views\footer();
    parent::__construct();
  }

  function testFooterHtml() {
    ob_start();
    $this->footerViews->html();
    $footer = trim( ob_get_clean() );
    $this->assertStringEndsWith( '</body></html>', $footer );
  }

  function testfooter() {
    $footer = $this->footerViews->footer('');
    $this->assertSame( '<footer class="footer navbar navbar-inverse bg-light fixed-bottom"><div class="container"><span class="text-muted">Powered by Bootstrap</span></div></footer>', $footer );
  }

  function testfooterClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\footer\class', function() { return 'footer-class-test'; } );
    $footer = $this->footerViews->footer('');
    $this->assertSame( '<footer class="footer-class-test"><div class="container"><span class="text-muted">Powered by Bootstrap</span></div></footer>', $footer );
  }

  function testfooterTextFilter() {
    add_filter( 'Boldface\Bootstrap\Views\footer\text', function() { return 'Footer test'; } );
    $footer = $this->footerViews->footer('');
    $this->assertSame( '<footer class="footer navbar navbar-inverse bg-light fixed-bottom"><div class="container">Footer test</div></footer>', $footer );
  }
}
