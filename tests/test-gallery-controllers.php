<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestGalleryControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->galleryViews = new \Boldface\Bootstrap\Views\gallery();
    $this->galleryModels = new \Boldface\Bootstrap\Models\gallery( $this->galleryViews );
    $this->galleryControllers = new \Boldface\Bootstrap\Controllers\gallery( $this->galleryModels );
    parent::__construct();
  }

  function testGallery() {
    $this->galleryControllers->wp();
    $this->assertSame( 5, has_filter( 'post_gallery', [ $this->galleryControllers->getModel(), 'gallery' ] ) );
  }
}
