<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestEntryControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->entryViews = new \Boldface\Bootstrap\Views\entry();
    $this->entryModels = new \Boldface\Bootstrap\Models\entry( $this->entryViews );
    $this->entryControllers = new \Boldface\Bootstrap\Controllers\entry( $this->entryModels );
    parent::__construct();
  }

  function testEntry() {
    $this->entryControllers->wp();
    $this->assertSame( 100, has_filter( 'the_content', [ $this->entryControllers->getModel(), 'autoRow' ] ) );
    $this->assertSame( 10, has_filter( 'post_class', [ $this->entryControllers->getModel(), 'post_class' ] ) );
    $this->assertSame( 5, has_filter( 'Boldface\Bootstrap\Views\entry', [ $this->entryControllers->getView(), 'entryHeader' ] ) );
  }
}
