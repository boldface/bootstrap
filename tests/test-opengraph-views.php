<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestOpenGraphViews extends \WP_UnitTestCase {

  function __construct() {
    $this->openGraphViews = new \Boldface\Bootstrap\Views\openGraph();
    parent::__construct();
  }

  function testOpenGraphTitle() {
    add_filter( 'the_title', function() { return 'openGraph title'; } );
    $this->openGraphViews->title();
    $this->expectOutputString( '<meta property="og:title" content="openGraph title" />' );
  }

  function testOpenGraphNoTitle() {
    add_filter( 'the_title', '__return_empty_string' );
    $this->openGraphViews->title();
    $this->expectOutputString( '' );
  }

  function testOpenGraphTitleFilter() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\title', function() { return 'openGraph filter title'; } );
    $this->openGraphViews->title();
    $this->expectOutputString( '<meta property="og:title" content="openGraph filter title" />' );
  }

  function testOpenGraphType() {
    $type = $this->openGraphViews->type();
    $this->expectOutputString( '<meta property="og:type" content="article" />' );
  }

  function testOpenGraphNoType() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\type', '__return_empty_string' );
    $this->openGraphViews->type();
    $this->expectOutputString( '' );
  }

  function testOpenGraphTypeFilter() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\type', function() { return 'openGraph'; } );
    $this->openGraphViews->type();
    $this->expectOutputString( '<meta property="og:type" content="openGraph" />' );
  }

  function testOpenGraphImage() {
    add_filter( 'get_site_icon_url', function() { return 'https://example.com/site-icon.png'; } );
    $this->openGraphViews->image();
    $this->expectOutputString( '<meta property="og:image" content="https://example.com/site-icon.png" />' );
  }

  function testOpenGraphNoImage() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\image', '__return_empty_string' );
    $this->openGraphViews->image();
    $this->expectOutputString( '' );
  }

  function testOpenGraphImageFilter() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\image', function() { return 'openGraph'; } );
    $this->openGraphViews->image();
    $this->expectOutputString( '<meta property="og:image" content="openGraph" />' );
  }

  function testOpenGraphImageType() {
    $imageType = $this->openGraphViews->imageType();
    $this->expectOutputString( '' );
  }

  function testOpenGraphNoImageType() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\image:type', '__return_empty_string' );
    $this->openGraphViews->imageType();
    $this->expectOutputString( '' );
  }

  function testOpenGraphImageTypeFilter() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\image:type', function() { return 'png'; } );
    $this->openGraphViews->imageType();
    $this->expectOutputString( '<meta property="og:image:type" content="png" />' );
  }

  function testOpenGraphNoDescription() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\description', '__return_empty_string' );
    $this->openGraphViews->description();
    $this->expectOutputString( '' );
  }

  function testOpenGraphDescriptionFilter() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\description', function() { return 'openGraph description'; } );
    $this->openGraphViews->description();
    $this->expectOutputString( '<meta property="og:description" content="openGraph description" />' );
  }

  function testOpenGraphLocale() {
    $locale = $this->openGraphViews->locale();
    $content = get_bloginfo( 'language' );
    $this->expectOutputString( '<meta property="og:locale" content="'.$content.'" />' );
  }

  function testOpenGraphNoLocale() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\locale', '__return_empty_string' );
    $this->openGraphViews->locale();
    $this->expectOutputString( '' );
  }

  function testOpenGraphLocaleFilter() {
    add_filter( 'Boldface\Bootstrap\Views\openGraph\locale', function() { return 'en-US'; } );
    $this->openGraphViews->locale();
    $this->expectOutputString( '<meta property="og:locale" content="en-US" />' );
  }
}
