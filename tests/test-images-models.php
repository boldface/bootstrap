<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestImagesModels extends \WP_UnitTestCase {

  function __construct() {
    $this->imagesViews = new \Boldface\Bootstrap\Views\images();
    $this->imagesModels = new \Boldface\Bootstrap\Models\images( $this->imagesViews );
    parent::__construct();
  }

  function testImagesGetImageTagClassLeft() {
    $class = $this->imagesModels->get_image_tag_class( '', 0, 'left', 150 );
    $this->assertSame( 'img img-fluid pr-3 float-left', $class );
  }

  function testImagesGetImageTagClassCenter() {
    $class = $this->imagesModels->get_image_tag_class( '', 0, 'center', 150 );
    $this->assertSame( 'img img-fluid mx-auto d-block', $class );
  }

  function testImagesGetImageTagClassRight() {
    $class = $this->imagesModels->get_image_tag_class( '', 0, 'right', 150 );
    $this->assertSame( 'img img-fluid pl-3 float-right', $class );
  }

  function testImagesGetImageTagClassDefault() {
    $class = $this->imagesModels->get_image_tag_class( '', 0, 'not-a-class', 150 );
    $this->assertSame( 'img img-fluid', $class );
  }

  function testImagesImageCaptionShortcode() {
    $shortcode = $this->imagesModels->img_caption_shortcode( '', [], '' );
    $this->assertSame( '<figure id="" class=""><figcaption></figcaption></figure>', $shortcode );
  }

  function testImagesImageCaptionShortcodeClassAlignLeft() {
    $shortcode = $this->imagesModels->img_caption_shortcode( '', [ 'align' => 'alignleft', ], '' );
    $this->assertSame( '<figure id="" class="float-left"><figcaption></figcaption></figure>', $shortcode );
  }

  function testImagesImageCaptionShortcodeID() {
    $shortcode = $this->imagesModels->img_caption_shortcode( '', [ 'id' => 'id', ], '' );
    $this->assertSame( '<figure id="id" class=""><figcaption></figcaption></figure>', $shortcode );
  }
}
