<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestNavigationModels extends \WP_UnitTestCase {

  function __construct() {
    $this->navigationViews = new \Boldface\Bootstrap\Views\navigation();
    $this->navigationModels = new \Boldface\Bootstrap\Models\navigation( $this->navigationViews );
    parent::__construct();
  }

  function testNavigationGetMenus() {
    $primaryMenu = esc_html__( 'Primary Menu', 'bootstrap' );
    $menus = $this->navigationModels->getMenus();
    $this->assertSame( [ 'primary' => $primaryMenu ], $menus );
  }

  function testNavigationGetMenusFilter() {
    add_filter( 'Boldface\Bootstrap\navigation\menus', function() { return [ 'primary' => 'Primary', ]; } );
    $menus = $this->navigationModels->getMenus();
    $this->assertSame( [ 'primary' => 'Primary' ], $menus );
  }

  function testNavigationNavMenuId() {
    $id = $this->navigationModels->nav_menu_item_id( 'some-string' );
    $this->assertSame( '', $id );
  }

  function testNavigationNavMenuItems() {
    $class = $this->navigationModels->nav_menu_items( [ 'class' ] );
    $this->assertSame( [ 'nav-item' ], $class );
  }

  function testNavigationNavMenuItemsActive() {
    $class = $this->navigationModels->nav_menu_items( [ 'current-menu-item' ] );
    $this->assertSame( [ 'active', 'nav-item' ], $class );
  }

  function testNavigationNavMenuLinkAttributes() {
    $attributes = $this->navigationModels->nav_menu_link_attributes([]);
    $this->assertSame( [ 'class' => 'nav-link', ], $attributes );
  }

  function testNavigationNavMenuLinkAttributesAlt() {
    $attributes = $this->navigationModels->nav_menu_link_attributes( [ 'class' => 'some class', ] );
    $this->assertSame( [ 'class' => 'nav-link', ], $attributes );
  }

  function testNavigationNavMenuSubmenuCssClass() {
    $class = $this->navigationModels->nav_menu_submenu_css_class([]);
    $this->assertSame( [ 'dropdown-menu', 'bg-inverse' ], $class );
  }

  function testNavigationNavMenuSubmenuCssClassAlt() {
    $class = $this->navigationModels->nav_menu_submenu_css_class( [ 'class' ] );
    $this->assertSame( [ 'class', 'dropdown-menu', 'bg-inverse' ], $class );
  }
}
