<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestContactForm7Controllers extends \WP_UnitTestCase {

  function __construct() {
    $this->contactForm7Views = new \Boldface\Bootstrap\Views\contactForm7();
    $this->contactForm7Models = new \Boldface\Bootstrap\Models\contactForm7( $this->contactForm7Views );
    $this->contactForm7Controllers = new \Boldface\Bootstrap\Controllers\contactForm7( $this->contactForm7Models );
    parent::__construct();
  }

  function testContactForm7WP() {
    $this->contactForm7Controllers->wp();
    $this->assertSame( 10, has_filter( 'wp_enqueue_scripts', [ $this->contactForm7Controllers->getModel(), 'dequeue' ] ) );
    $this->assertSame( 10, has_filter( 'wpcf7_form_class_attr', [ $this->contactForm7Controllers->getModel(), 'class_attr' ] ) );
    $this->assertSame( 10, has_filter( 'Boldface\Bootstrap\Views\contactForm7\elements', [ $this->contactForm7Controllers->getModel(), 'elements' ] ) );
    $this->assertSame( 10, has_filter( 'wpcf7_form_elements', [ $this->contactForm7Controllers->getView(), 'elements' ] ) );
  }
}
