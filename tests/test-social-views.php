<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestSocialViews extends \WP_UnitTestCase {

  function __construct() {
    $this->socialViews = new \Boldface\Bootstrap\Views\social();
    parent::__construct();
  }

  function testSocial() {
    $this->socialViews->html();
    $this->expectOutputString( '<aside class="container social"></aside>' );
  }

  function testSocialFilter() {
    add_filter( 'Boldface\Bootstrap\Views\social', function() { return 'Social content'; } );
    $this->socialViews->html();
    $this->expectOutputString( '<aside class="container social">Social content</aside>' );
  }

  function testSocialClassFilter() {
    add_filter( 'Boldface\Bootstrap\Views\social\class', function() { return 'social-class-test'; } );
    $this->socialViews->html();
    $this->expectOutputString( '<aside class="social-class-test"></aside>' );
  }
}
