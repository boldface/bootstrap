<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestModalModels extends \WP_UnitTestCase {

  function __construct() {
    $this->modalViews = new \Boldface\Bootstrap\Views\modal();
    $this->modalModels = new \Boldface\Bootstrap\Models\modal( $this->modalViews );
    parent::__construct();
  }

  function testModalTriggerAtts() {
    $atts = $this->modalModels->triggerAtts([]);
    $this->assertSame( [
      'type' => 'button',
      'class' => 'btn btn-primary',
      'data-toggle' => 'modal',
      'data-target' => '#exampleModal',
    ], $atts );
  }

  function testModalTriggerText() {
    $atts = $this->modalModels->triggerText('');
    $this->assertSame( 'Launch demo modal', $atts );
  }

  function testModalAtts() {
    $atts = $this->modalModels->modalAtts([]);
    $this->assertSame( [
      'class'           => 'modal fade',
      'id'              => 'exampleModal',
      'tabindex'        => '-1',
      'role'            => 'dialog',
      'aria-labelledby' => 'exampleModalLabel',
      'aria-hidden'     => 'true',
    ], $atts );
  }
}
