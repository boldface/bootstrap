<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestFeaturedImageViews extends \WP_UnitTestCase {

  function __construct() {
    $this->featuredImageViews = new \Boldface\Bootstrap\Views\featuredImage();
    parent::__construct();
  }

  function testfeaturedImage() {
    $featuredImage = $this->featuredImageViews->defaultImage();
    $this->expectOutputString( '<div class="row"><div class="post-thumbnail" style=""></div></div>' );
  }

  function testfeaturedImageWrapperClassFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage\wrapper', '__return_empty_string' );
    $featuredImage = $this->featuredImageViews->defaultImage();
    $this->expectOutputString( '<div class=""><div class="post-thumbnail" style=""></div></div>' );
  }

  function testfeaturedImageClassFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage\class', '__return_empty_string' );
    $featuredImage = $this->featuredImageViews->defaultImage();
    $this->expectOutputString( '<div class="row"><div class="" style=""></div></div>' );
  }

  function testfeaturedImageStyleFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage\style', function() { return 'display:none;'; } );
    $featuredImage = $this->featuredImageViews->defaultImage();
    $this->expectOutputString( '<div class="row"><div class="post-thumbnail" style="display:none;"></div></div>' );
  }

  function testfeaturedImageFilter() {
    \add_filter( 'Boldface\Bootstrap\Views\featuredImage', function() { return 'Featured Image'; } );
    $featuredImage = $this->featuredImageViews->defaultImage();
    $this->expectOutputString( '<div class="row"><div class="post-thumbnail" style="">Featured Image</div></div>' );
  }
}
