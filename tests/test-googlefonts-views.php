<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestGoogleFontsViews extends \WP_UnitTestCase {

  function __construct() {
    $this->googleFontsViews = new \Boldface\Bootstrap\Views\googleFonts();
    parent::__construct();
  }

  function testGoogleFontsLink() {
    $link = $this->googleFontsViews->link();
    $this->expectOutputString( '<link href="//fonts.googleapis.com/css?family=" rel="stylesheet">', $link );
  }

  function testGoogleFontsSetFontsLink() {
    $this->googleFontsViews->setFonts( [ 'test' ] );
    $link = $this->googleFontsViews->link();
    $this->expectOutputString( '<link href="//fonts.googleapis.com/css?family=test" rel="stylesheet">', $link );
  }

  function testGoogleFontsSetFontsLinkMultiple() {
    $this->googleFontsViews->setFonts( [ 'test', 'futura' ] );
    $link = $this->googleFontsViews->link();
    $this->expectOutputString( '<link href="//fonts.googleapis.com/css?family=test|futura" rel="stylesheet">', $link );
  }
}
