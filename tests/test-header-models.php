<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestHeaderModels extends \WP_UnitTestCase {

  function __construct() {
    $this->headerViews = new \Boldface\Bootstrap\Views\header();
    $this->headerModels = new \Boldface\Bootstrap\Models\header( $this->headerViews );
    parent::__construct();
  }

  function testBodyClass() {
    $bodyClass = $this->headerModels->bodyClass( [ 'test', 'id-test', 'test-id', 'template-test', 'test-template' ] );
    $this->assertEquals( [ 'test' ], $bodyClass );
  }

  function testSingularDescription() {
    $singularDescription = $this->headerModels->singularDescription( '' );
    $this->assertEquals( true, is_string( $singularDescription ) );
  }
}
