<?php

/**
 * @package Boldface\Bootstrap
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap\Tests;

class TestGoogleFontsControllers extends \WP_UnitTestCase {

  function __construct() {
    $this->googleFontsViews = new \Boldface\Bootstrap\Views\googleFonts();
    $this->googleFontsModels = new \Boldface\Bootstrap\Models\googleFonts( $this->googleFontsViews );
    $this->googleFontsControllers = new \Boldface\Bootstrap\Controllers\googleFonts( $this->googleFontsModels );
    parent::__construct();
  }

  function testGoogleFontsWP() {
    $this->googleFontsControllers->wp();
    $this->assertFalse( has_filter( 'wp_footer', [ $this->googleFontsControllers->getView(), 'link' ] ) );
  }

  function testGoogleFontsWPFilter() {
    add_filter( 'Boldface\Bootstrap\Models\googleFonts', function() { return [ 'test' ]; } );
    $this->googleFontsControllers->wp();
    $this->assertEquals( 10, has_filter( 'wp_footer', [ $this->googleFontsControllers->getView(), 'link' ] ) );
  }
}
