<?php
/**
 * Bootstrap Theme
 *
 * @package Boldface\Bootstrap
 *
 * @link    https://gitlab.com/boldface/bootstrap/
 * @version 1.1.2
 * @author  Boldface Design Group
 * @link    http://www.boldfacedesign.com/
 * @license GNU General Public License v3 or later
 * @link    https://www.gnu.org/licenses/gpl-3.0.html
 */
