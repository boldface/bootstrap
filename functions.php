<?php

/**
 * @package Boldface\Bootstrap
 *
 * This is the first Bootstrap theme file that is seen by WordPress. At this
 * point, WordPress is in-between hooks. The first hook available to themes is
 * after_setup_theme. All this file does is require the theme initiator and
 * hook into WordPress on after_setup_theme to initialize the theme.
 *
 * If a child theme is in use, its functions.php file is loaded immediately
 * prior to this file.
 */
declare( strict_types = 1 );
namespace Boldface\Bootstrap;

require __DIR__ . '/src/theme.php';
\add_action( 'after_setup_theme', [ new theme(), 'init' ], 0 );
